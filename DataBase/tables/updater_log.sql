--liquibase formatted sql
--changeset vait:02_updater_log
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('updater_log') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `updater_log` (
	`log_id` INT NOT NULL AUTO_INCREMENT,
	`event_name` VARCHAR(20) CHARACTER SET utf8 NOT NULL,
	`event_date` DATE NOT NULL,
	`event_message` VARCHAR(200) CHARACTER SET utf8 NOT NULL,
	PRIMARY KEY (`log_id`)
) ENGINE=InnoDB;