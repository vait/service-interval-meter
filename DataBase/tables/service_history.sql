--liquibase formatted sql
--changeset vait:01_service_history
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('service_history') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `service_history` (
    `service_history_id` BIGINT NOT NULL AUTO_INCREMENT,
    `equipment_id` INT NOT NULL,
    `last_service_date` DATE NOT NULL,
    UNIQUE KEY `u_unique_service` (`equipment_id`,`last_service_date`) USING BTREE,
    PRIMARY KEY (`service_history_id`),
    constraint fk_sv_hist_equipment foreign key(equipment_id) references equipment(equipment_id)
) ENGINE=InnoDB;
