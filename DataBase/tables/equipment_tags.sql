--liquibase formatted sql
--changeset vait:01_equipment_tags
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('equipment_tags') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `equipment_tags` (
    `tag_id` INT NOT NULL,
    `equipment_id` INT NOT NULL,
    PRIMARY KEY (`tag_id`,`equipment_id`),
    constraint fk_eq_tag_tag foreign key(tag_id) references tag(tag_id),
    constraint fk_eq_tag_equipment foreign key(equipment_id) references equipment(equipment_id)
) ENGINE=InnoDB;

