--liquibase formatted sql
--changeset vait:01_equipment_activity_type
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('equipment_activity_type') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `equipment_activity_type` (
    `equipment_id` INT NOT NULL,
    `activity_type_id` INT NOT NULL,
    PRIMARY KEY (`equipment_id`,`activity_type_id`),
    constraint fk_eq_act_tp_equipment foreign key(equipment_id) references equipment(equipment_id),
    constraint fk_eq_act_tp_activity_type foreign key(activity_type_id) references activity_type(activity_type_id)
) ENGINE=InnoDB;


