--liquibase formatted sql
--changeset vait:01_activity_tags
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('activity_tags') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `activity_tags` (
    `activity_id` BIGINT NOT NULL,
    `tag_id` INT NOT NULL,
    PRIMARY KEY (`activity_id`, `tag_id`),
    constraint fk_act_tag_activity foreign key(activity_id) references activity(activity_id),
    constraint fk_act_tag_tag foreign key(tag_id) references tag(tag_id)
) ENGINE=InnoDB;

