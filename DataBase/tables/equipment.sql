--liquibase formatted sql
--changeset vait:01_equipment
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('equipment') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `equipment` (
    `equipment_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    UNIQUE KEY `u_name` (`name`) USING BTREE,
    PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB;
