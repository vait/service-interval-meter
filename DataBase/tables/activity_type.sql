--liquibase formatted sql
--changeset vait:01_activity_type
--runOnChange:true
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.TABLES WHERE UPPER(TABLE_NAME) = UPPER('activity_type') AND TABLE_SCHEMA in (SELECT DATABASE())

CREATE TABLE `activity_type` (
	`activity_type_id` INT NOT NULL AUTO_INCREMENT,
	`activity_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	UNIQUE KEY `u_activity_type` (`activity_type`) USING BTREE,
	PRIMARY KEY (`activity_type_id`)
) ENGINE=InnoDB;

