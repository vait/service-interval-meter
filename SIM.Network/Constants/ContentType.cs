namespace SIM.Network.Constants
{
	internal static class ContentType
	{
		public const string Json = "application/json";
		public const string UrlFormEncoded = "application/x-www-form-urlencoded";

	}
}
