namespace SIM.Network.Constants
{
	internal static class Uri
	{
		public static class SportTracker
		{
			public static System.Uri UserAuthorize = new System.Uri("https://api.sports-tracker.com/apiserver/v1/login?source=javasc");
			public static System.Uri Workouts = new System.Uri("https://api.sports-tracker.com/apiserver/v1/workouts?limited=true&limit=1000000");

		}
	}
}
