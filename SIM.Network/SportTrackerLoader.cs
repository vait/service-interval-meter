﻿using Microsoft.Net.Http.Headers;
using SIM.Core;
using SIM.Core.Models;
using SIM.Network.Models.SportTracker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using CacheControlHeaderValue = System.Net.Http.Headers.CacheControlHeaderValue;

namespace SIM.Network
{
	public class SportTrackerLoader : BaseLoader
	{
		private string _authKey = "";

		protected override IMovesParser Parser => new SportTrackerParser();
		protected override HttpClient CreateHttpClient()
		{
			var handler = new HttpClientHandler
			{
				UseCookies = true,
				AllowAutoRedirect = true,
				CookieContainer = new CookieContainer(),
				AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
			};

			var client = new HttpClient(handler);

			client.DefaultRequestHeaders.Add("DNT", "1");
			client.DefaultRequestHeaders.Add(HeaderNames.Pragma, "no-cache");
			client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue { NoCache = true };

			return client;
		}

		protected override async Task<Move[]> LoadInternalAsync(DateTimeOffset? startPeriod, DateTimeOffset? endPeriod, CancellationToken ct)
		{
			var moves = Array.Empty<Move>();

			var headers = new Dictionary<string, string>
			{
				{"STTAuthorization", _authKey}
			};

			await ExecuteRequestAsync(
			Constants.Uri.SportTracker.Workouts,
			HttpMethod.Get,
			null,
			ct,
			headers,
			async (stream, innerCt) =>
			{
				using (var reader = new StreamReader(stream))
				{
					moves = await Parser.GetMoviesAsync(reader, startPeriod, endPeriod, innerCt).ConfigureAwait(false);
				}
			}).ConfigureAwait(false);

			return moves;
		}

		protected override async Task<bool> LoginAsync(string login, string password, CancellationToken ct)
		{
			var headers = new Dictionary<string, string>
			{
				{HeaderNames.Accept, "application/json,text/plain" },
			};

			var httpContent = new FormUrlEncodedContent(new[] {
					new KeyValuePair<string, string>("l", login),
					new KeyValuePair<string, string>("p", password),

				});
			httpContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue(Constants.ContentType.UrlFormEncoded);

			SportTrackerAuthResponse authResult = null;

			await ExecuteRequestAsync(Constants.Uri.SportTracker.UserAuthorize,
				HttpMethod.Post,
				httpContent,
				ct,
				headers,
				async (stream, innerCt) =>
				{
					authResult = await JsonSerializer.DeserializeAsync<SportTrackerAuthResponse>(stream, null, innerCt).ConfigureAwait(false);
				}).ConfigureAwait(false);

			if (authResult.Error == null)
			{
				_authKey = authResult.SessionKey;
				return true;
			}

			throw new AuthenticationException($"[{authResult.Error.Code}] {authResult.Error.Description}");
		}

		//https://api.sports-tracker.com/apiserver/v1/workouts?limited=true&limit=1000000
		//https://api.sports-tracker.com/apiserver/v1/user/feed/combined?limit=10&offset=0

	}
}
