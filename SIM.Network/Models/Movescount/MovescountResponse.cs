﻿namespace SIM.Network.Models.Movescount
{
	internal class MovescountResponse
	{
		public InnerData d { get; set; }
	}

	internal class InnerData
	{
		public string Error { get; set; }

		public string Value { get; set; }
	}
}
