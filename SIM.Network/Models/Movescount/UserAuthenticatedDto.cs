using System;

namespace SIM.Network.Models.Movescount
{
	internal class UserAuthenticatedDto
	{
		public string Token { get; set; }
		public string UtcOffset { get; set; } = TimeZoneInfo.Local.BaseUtcOffset.TotalMinutes.ToString();
		public string RedirectUri { get; set; } = "/latestmove";
	}
}