namespace SIM.Network.Models.Movescount
{
	internal class AuthenticateDto
	{
		public string EmailAddress { get; set; }
		public string Password { get; set; }
	}
}