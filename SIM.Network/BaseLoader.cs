﻿using SIM.Core;
using SIM.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace SIM.Network
{
	public abstract class BaseLoader : IDisposable
	{
		protected abstract IMovesParser Parser { get; }

		protected readonly Lazy<HttpClient> _httpClient;
		protected HttpClient HttpClient => _httpClient.Value;

		protected BaseLoader()
		{
			_httpClient = new Lazy<HttpClient>(CreateHttpClient, LazyThreadSafetyMode.ExecutionAndPublication);
		}

		protected abstract HttpClient CreateHttpClient();

		protected abstract Task<bool> LoginAsync(string login, string password, CancellationToken ct);
		protected abstract Task<Move[]> LoadInternalAsync(DateTimeOffset? startPeriod, DateTimeOffset? endPeriod, CancellationToken ct);

		public async Task<Move[]> LoadAsync(string login, string password, DateTimeOffset? startPeriod, DateTimeOffset? endPeriod, CancellationToken ct)
		{
			bool logged;
			try
			{
				logged = await LoginAsync(login, password, ct).ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				throw new AuthenticationException("Check login/pass pair", ex);
			}

			if (!logged)
			{
				throw new AuthenticationException("Check login/pass pair");
			}

			return await LoadInternalAsync(startPeriod, endPeriod, ct).ConfigureAwait(false);
		}

		public void Dispose()
		{
			if (_httpClient != null && _httpClient.IsValueCreated)
			{
				_httpClient.Value?.Dispose();
			}
		}

		protected async Task ExecuteRequestAsync(Uri uri,
			HttpMethod method,
			HttpContent httpContent,
			CancellationToken ct,
			IReadOnlyDictionary<string, string> headers = null,
			Func<Stream, CancellationToken, Task> callBack = null)
		{
			using (var requestMessage = new HttpRequestMessage(method, uri))
			{
				if (httpContent != null)
				{
					requestMessage.Content = httpContent;
				}

				if (headers != null)
				{
					foreach (var header in headers)
					{
						requestMessage.Headers.Add(header.Key, header.Value);
					}
				}

				using (var response = await HttpClient.SendAsync(requestMessage, ct)
					.ConfigureAwait(false))
				{
					if (response.IsSuccessStatusCode)
					{
						if (callBack != null)
						{
							var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
							await callBack(stream, ct).ConfigureAwait(false);
						}

						return;
					}

					var errContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					var errorMessage = $"[{uri}] {(int)response.StatusCode} ({response.ReasonPhrase}): {errContent}";

					throw new HttpRequestException(errorMessage);
				}
			}
		}

	}
}
