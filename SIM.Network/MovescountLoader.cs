﻿using Microsoft.Net.Http.Headers;
using SIM.Core;
using SIM.Core.Models;
using SIM.Network.Models.Movescount;
using SIM.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using CacheControlHeaderValue = System.Net.Http.Headers.CacheControlHeaderValue;

namespace SIM.Network
{
	public class MovescountLoader : BaseLoader
	{
		protected override IMovesParser Parser => new MovescountParser();

		protected override HttpClient CreateHttpClient()
		{
			var handler = new HttpClientHandler
			{
				UseCookies = true,
				AllowAutoRedirect = true,
				CookieContainer = new CookieContainer(),
				AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
			};

			var client = new HttpClient(handler);

			client.DefaultRequestHeaders.Add("DNT", "1");
			client.DefaultRequestHeaders.Add(HeaderNames.Pragma, "no-cache");
			client.DefaultRequestHeaders.Add(HeaderNames.AcceptEncoding, "gzip, deflate, br");
			client.DefaultRequestHeaders.Add(HeaderNames.AcceptLanguage, "en-US,en;q=0.5");
			client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue { NoCache = true };

			return client;
		}

		protected override async Task<bool> LoginAsync(string login, string password, CancellationToken ct)
		{
			var token = await GetTokenAsync(login, password, ct).ConfigureAwait(false);
			await PreLoadDataAsync(ct).ConfigureAwait(false);
			return await UserAuthenticatedAsync(token, ct).ConfigureAwait(false);
		}

		protected override async Task<Move[]> LoadInternalAsync(DateTimeOffset? startPeriod, DateTimeOffset? endPeriod, CancellationToken ct)
		{
			var moves = Array.Empty<Move>();
			await ExecuteRequestAsync(
				Constants.Uri.Movescount.MoveList,
				HttpMethod.Get,
				null,
				ct,
				null,
				async (stream, innerCt) =>
				{
					using (var reader = new StreamReader(stream))
					{
						moves = await Parser.GetMoviesAsync(reader, startPeriod, endPeriod, innerCt).ConfigureAwait(false);
					}
				}).ConfigureAwait(false);

			return moves;
		}

		private Task PreLoadDataAsync(CancellationToken ct)
		{
			var headers = new Dictionary<string, string>
			{
				{"Upgrade-Insecure-Requests", "1"},
				{HeaderNames.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}
			};
			return ExecuteRequestAsync(Constants.Uri.Movescount.PreAuth, HttpMethod.Get, null, ct, headers);
		}

		private async Task<string> GetTokenAsync(string login, string password, CancellationToken ct)
		{
			var headers = new Dictionary<string, string>
			{
				{HeaderNames.Accept, "application/json, text/javascript, */*; q=0.01"}
			};
			var authData = new AuthenticateDto
			{
				Password = password,
				EmailAddress = login
			};

			string token = null;

			var httpContent = new StringContent(JsonSerializer.Serialize(authData));
			httpContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue(Constants.ContentType.Json);

			await ExecuteRequestAsync(
				Constants.Uri.Movescount.UserAuthorityService,
				HttpMethod.Post,
				httpContent,
				ct,
				headers,
				async (stream, innerCt) =>
				{
					token = await JsonSerializer.DeserializeAsync<string>(stream, null, innerCt).ConfigureAwait(false);
				}).ConfigureAwait(false);

			return token;
		}

		private async Task<bool> UserAuthenticatedAsync(string token, CancellationToken ct)
		{
			var headers = new Dictionary<string, string>
			{
				{HeaderNames.Accept, "application/json,text/plain" },
			};

			var authenticatedDto = new UserAuthenticatedDto
			{
				Token = token
			};

			MovescountResponse authResult = null;

			var httpContent = new StringContent(JsonSerializer.Serialize(authenticatedDto, JsonUtils.CameCasePolice));
			httpContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue(Constants.ContentType.Json);

			await ExecuteRequestAsync(Constants.Uri.Movescount.UserAuthenticated,
				HttpMethod.Post,
				httpContent,
				ct,
				headers,
				async (stream, innerCt) =>
				{
					authResult = await JsonSerializer.DeserializeAsync<MovescountResponse>(stream, null, innerCt).ConfigureAwait(false);
				}).ConfigureAwait(false);

			return string.IsNullOrEmpty(authResult.d.Error);
		}
	}
}
