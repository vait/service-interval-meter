using SIM.CoreOld.Helpers;
using SIM.CoreOld.Models.Raw.Movescount;
using SIM.CoreOld.Models.Raw.Movescount.Schema;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SIM.CoreOld.JsonConverters
{
	internal class MoviescountConverter : JsonConverter<Move[]>
	{
		private readonly DateTimeOffset _dateFrom;
		private readonly DateTimeOffset _dateTo;

		public MoviescountConverter(DateTimeOffset? dateFrom = null, DateTimeOffset? dateTo = null)
		{
			_dateFrom = dateFrom ?? DateTimeOffset.MinValue;
			_dateTo = dateTo ?? DateTimeOffset.Now;

		}

		public override Move[] Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			if (reader.TokenType != JsonTokenType.StartObject)
			{
				throw new JsonException();
			}

			var list = new List<Move>();
			Schema schema = null;
			var moveRecords = Array.Empty<MoveRecord>();

			while (reader.Read())
			{
				if (reader.TokenType == JsonTokenType.EndObject)
				{
					break;
				}

				// Get the key.
				if (reader.TokenType != JsonTokenType.PropertyName)
				{
					throw new JsonException();
				}

				var propertyName = reader.GetString();

				if (propertyName == DataConstants.Schema)
				{
					schema = JsonSerializer.Deserialize<Schema>(ref reader, options);
					continue;
				}

				if (propertyName == DataConstants.Moves)
				{
					moveRecords = JsonSerializer.Deserialize<MoveRecord[]>(ref reader, options);
				}
			}

			if (schema == null)
			{
				throw new NullReferenceException(nameof(schema));
			}

			foreach (var record in moveRecords)
			{
				var startDate = schema.GetStartDate(record);

				if (startDate <= _dateFrom || _dateTo <= startDate)
				{
					continue;
				}

				var move = new Move
				{
					// ReSharper disable BuiltInTypeReferenceStyle
					MoveId = schema.GetFieldValue<int>(FieldName.MoveId, record).ToString(),
					ActivityType = (ActivityType)schema.GetFieldValue<int>(FieldName.ActivityId, record),
					// ReSharper restore BuiltInTypeReferenceStyle
					Distance = schema.GetFieldValue<int>(FieldName.Distance, record),
					Duration = schema.GetTimeSpan(FieldName.Duration, record),
					Tags = new HashSet<string>(schema.GetArray<string>(FieldName.Tags, record)),
					StartDate = startDate
				};

				list.Add(move);
			}
			return list.ToArray();
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Move[]);
		}

		public override void Write(Utf8JsonWriter writer, Move[] value, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}
	}
}