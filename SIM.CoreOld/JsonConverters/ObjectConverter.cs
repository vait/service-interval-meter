using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SIM.CoreOld.JsonConverters
{
	internal class ObjectConverter : JsonConverter<object>
	{
		public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			if (reader.TokenType == JsonTokenType.True)
			{
				return true;
			}

			if (reader.TokenType == JsonTokenType.False)
			{
				return false;
			}

			if (reader.TokenType == JsonTokenType.Number)
			{
				if (reader.TryGetInt64(out var l))
				{
					return l;
				}

				return reader.GetDouble();
			}

			if (reader.TokenType == JsonTokenType.String)
			{
				if (reader.TryGetDateTime(out var datetime))
				{
					return datetime;
				}

				return reader.GetString();
			}

			if (reader.TokenType == JsonTokenType.StartArray)
			{
				return JsonSerializer.Deserialize<object[]>(ref reader, options);
			}

			using (var document = JsonDocument.ParseValue(ref reader))
			{
				return document.RootElement.Clone();
			}
		}

		public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}
	}
}