using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using SIM.CoreOld.Models.Raw.Movescount.Schema;

namespace SIM.CoreOld.JsonConverters
{
	internal class SchemaConverter : JsonConverter<Schema>
	{
		public override Schema Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			return new Schema(JsonSerializer.Deserialize<Dictionary<string, MoveFieldType>>(ref reader, options));
		}

		public override void Write(Utf8JsonWriter writer, Schema value, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}

		public override bool CanConvert(Type typeToConvert)
		{
			if (typeToConvert != typeof(Schema))
			{
				return false;
			}

			return true;
		}
	}
}