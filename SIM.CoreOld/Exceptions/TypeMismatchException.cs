using System;

namespace SIM.CoreOld.Exceptions
{
	public class TypeMismatchException : Exception
	{
		public Type OneType { get; }
		public Type SecondType { get; }

		public int FieldIndex { get; }

		public TypeMismatchException(Type oneType, Type secondType, int fieldIndex)
		{
			OneType = oneType;
			SecondType = secondType;
			FieldIndex = fieldIndex;
		}
	}
}