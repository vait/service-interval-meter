﻿using Microsoft.Extensions.DependencyInjection;

namespace SIM.CoreOld.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection WithCore(this IServiceCollection serviceCollection)
		{
			return serviceCollection
				.AddScoped<IMovesParser, SportTrackerParser>()
				;
		}
	}
}
