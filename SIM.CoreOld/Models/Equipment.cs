using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading;

namespace SIM.CoreOld.Models
{
	/// <summary>
	/// Оборудование
	/// </summary>
	public class Equipment
	{
		private Lazy<DateTimeOffset> _lastServiceDate;
		private Service[] _serviceHistory;

		/// <summary>
		/// Вид спорта.
		/// </summary>
		public ActivityType ActivityType { get; set; }

		/// <summary>
		/// Тэги
		/// </summary>
		public HashSet<string> Tags { get; set; }

		/// <summary>
		/// Название
		/// </summary>
		public string Name { get; set; }

		//public Service[] ServiceHistory { get; set; }

		public Service[] ServiceHistory
		{
			get
			{
				return _serviceHistory;
			}
			set
			{
				_serviceHistory = value;
				UpdateLastServiceDate();
			}
		}

		/// <summary>
		/// Дата последнего технического обслужвания.
		/// </summary>
		[JsonIgnore]
		public DateTimeOffset LastServiceDate => _lastServiceDate.Value;

		public Equipment()
		{
			UpdateLastServiceDate();
		}

		private void UpdateLastServiceDate()
		{
			if (ServiceHistory == null)
			{
				_lastServiceDate = new Lazy<DateTimeOffset>(() => Service.Default.LastServiceDate, LazyThreadSafetyMode.ExecutionAndPublication);
				return;
			}
			_lastServiceDate = new Lazy<DateTimeOffset>(
				() => ServiceHistory.DefaultIfEmpty(Service.Default).Max(x => x.LastServiceDate),
				LazyThreadSafetyMode.ExecutionAndPublication);
		}
	}
}
