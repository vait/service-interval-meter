﻿using System;

namespace SIM.CoreOld.Models
{
	/// <summary>
	/// Информация о сервисном обслуживании.
	/// </summary>
	public class Service
	{
		public static Service Default = new Service { LastServiceDate = DateTimeOffset.MinValue };

		/// <summary>
		/// Дата последнего технического обслужвания.
		/// </summary>
		public DateTimeOffset LastServiceDate { get; set; }
	}
}
