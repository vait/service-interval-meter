using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text.Json.Serialization;

namespace SIM.CoreOld.Models.Raw.Movescount.Schema
{
	/// <summary>
	/// Схема Move.
	/// </summary>
	[DebuggerDisplay("Count = {Count}")]
	[JsonConverter(typeof(JsonConverter<Dictionary<string, MoveFieldType>>))]
	internal class Schema : ReadOnlyDictionary<string, MoveFieldType>
	{
		public Schema(IDictionary<string, MoveFieldType> dictionary) : base(dictionary)
		{
		}
	}
}