using System.Diagnostics;

namespace SIM.CoreOld.Models.Raw.Movescount.Schema
{
	/// <summary>
	/// Описание поля схемы.
	/// </summary>
	[DebuggerDisplay("{Type}")]
	internal class MoveFieldType
	{
		/// <summary>
		/// Индекс в списке.
		/// </summary>
		public int Index { get; set; }

		/// <summary>
		/// Тип поля.
		/// </summary>
		public FieldType Type { get; set; }
	}
}