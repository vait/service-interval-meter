using SIM.Core.Attributes;
using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace SIM.CoreOld.Models.Raw.Movescount.Schema
{
	[JsonConverter(typeof(JsonStringEnumMemberConverter))]
	internal enum FieldType
	{
		[SystemType(typeof(byte))]
		Byte,

		[SystemType(typeof(DateTime))]
		Datetime,

		[SystemType(typeof(decimal))]
		Decimal,

		[SystemType(typeof(double))]
		Double,

		[SystemType(typeof(int))]
		Int32,

		[SystemType(typeof(float))]
		Single,

		[SystemType(typeof(string))]
		String,

		[EnumMember(Value = "string[]")]
		[SystemType(typeof(string[]))]
		StringArray,

		[SystemType(typeof(TimeSpan))]
		Timespan
	}
}