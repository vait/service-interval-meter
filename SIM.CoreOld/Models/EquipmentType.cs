namespace SIM.CoreOld.Models
{
	/// <summary>
	/// Тип оборудования.
	/// </summary>
	public class EquipmentType
	{
		public string Name { get; set; }
	}
}
