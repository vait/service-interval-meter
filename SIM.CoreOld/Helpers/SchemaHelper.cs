//using Newtonsoft.Json.Linq;
using SIM.Core.Exceptions;
using SIM.CoreOld.Helpers;
using SIM.CoreOld.Models.Raw.Movescount;
using SIM.CoreOld.Models.Raw.Movescount.Schema;
using SIM.Utils.Helpers;
using System;
using System.Linq;

namespace SIM.CoreOld.Helpers
{
	internal static class SchemaHelper
	{
		public static DateTimeOffset GetStartDate(this Schema schema, MoveRecord record)
		{
			var year = schema.GetFieldValue<int>(FieldName.Year, record);
			var month = schema.GetFieldValue<int>(FieldName.Month, record);
			var day = schema.GetFieldValue<int>(FieldName.Day, record);

			return new DateTimeOffset(year, month, day, 0, 0, 0, TimeSpan.Zero);
		}

		public static T GetFieldValue<T>(this Schema schema, string fieldName, MoveRecord record)
		{
			var field = schema[fieldName];
			return field.ConvertFieldWithCheck<T>(record);
		}

		public static TimeSpan GetTimeSpan(this Schema schema, string fieldName, MoveRecord record)
		{
			var field = schema[fieldName];
			field.GetAndCheckSystemType<TimeSpan>();
			return TimeSpan.FromMilliseconds((long)record[field.Index]);
		}

		public static T[] GetArray<T>(this Schema schema, string fieldName, MoveRecord record)
		{
			var field = schema[fieldName];
			field.GetAndCheckSystemType<T[]>();

			if (record[field.Index] == null)
			{
				return Array.Empty<T>();
			}

			if (!(record[field.Index] is object[] objectArray))
			{
				throw new TypeMismatchException(typeof(object[]), record[field.Index].GetType(), field.Index);
			}

			return objectArray.Cast<T>().ToArray();
		}

		private static T ConvertFieldWithCheck<T>(this MoveFieldType field, MoveRecord record)
		{
			var systemType = field.GetAndCheckSystemType<T>();
			try
			{
				return (T)Convert.ChangeType(record[field.Index], systemType);
			}
			catch (Exception ex)
			{
				throw new Exception($"Cant convert field [{field.Index}] to {typeof(T)}. Rcord[{field.Index}] = {record[field.Index]}", ex);
			}
		}

		private static Type GetAndCheckSystemType<T>(this MoveFieldType field)
		{
			var systemTypeAttribute = field.Type.GetAttribute<SystemTypeAttribute>();
			var systemType = systemTypeAttribute.Type;

			if (typeof(T) != systemType)
			{
				throw new TypeMismatchException(typeof(T), systemType, field.Index);
			}

			return systemType;
		}
	}
}