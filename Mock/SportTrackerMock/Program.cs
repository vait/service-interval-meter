using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SIM.Infrastructure;
using SIM.Infrastructure.IoC;
using System.IO;

internal class Program
{
	private static void Main(string[] args)
	{
		var builder = WebApplication.CreateBuilder(args);

		// Add services to the container.

		builder.Services.AddControllers();
		// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
		builder.Services.AddEndpointsApiExplorer();
		builder.Services.AddSwaggerGen();
		builder.Services.UseSIMInfrastructure();
		builder.Logging.ClearProviders();
		builder.Logging.AddConsole();
		builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
		{
			//config.Sources.Clear();
			config.SetBasePath(Directory.GetCurrentDirectory());

			config
				.AddJsonFile($"appsettings.json", false, false)
				.AddSharedJsonFile(hostingContext, 2);
		});

		var app = builder.Build();

		// Configure the HTTP request pipeline.
		if (app.Environment.IsDevelopment())
		{
			app.UseSwagger();
			app.UseSwaggerUI();
		}


		app.UseAuthorization();

		app.MapControllers();

		app.Run();
	}
}