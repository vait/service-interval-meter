using DataLoader.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIM.Infrastructure;
using System;
using System.IO;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using DataLoaderConstans = DataLoader.Constants;

namespace WebMock.Controllers
{
	public class AuthenticateDto
	{
		public string l { get; set; }
		public string p { get; set; }
		public string captchaToken { get; set; }
	}

	internal class SportsTrackerConSportsTrackerConfigurationMock : SportsTrackerConfiguration
	{
		public string CaptchaToken { get; set; } = string.Empty;
		public string Token { get; set; } = string.Empty;
	}

	public class SportTrackerAuthResponse
	{
		[JsonPropertyName("sessionkey")]
		public string SessionKey { get; set; }

	}

	[ApiController]
	public class MainController : ControllerBase
	{
		private readonly ILogger _logger;
		private readonly ISIMConfigurationService _configuration;
		private static string _sessionKey = Guid.NewGuid().ToString();

		public MainController(
			ILogger<MainController> logger,
			ISIMConfigurationService configuration)
		{
			_logger = logger;
			_configuration = configuration;
		}

		[HttpPost]
		[Route("apiserver/v1/login")]
		public Task<SportTrackerAuthResponse> UserAuthenticate([FromForm] AuthenticateDto data)
		{
			_logger.LogInformation(nameof(UserAuthenticate) + " {0}", data);

			var config = _configuration.Get<SportsTrackerConSportsTrackerConfigurationMock>(DataLoaderConstans.Configuration.SportsTrackerSection);

			if (data.l == config.Login && data.p == config.Password && data.captchaToken == config.CaptchaToken)
			{
				return Task.FromResult(new SportTrackerAuthResponse
				{
					SessionKey = _sessionKey
				});
			}

			throw new UnauthorizedAccessException();
		}

		[HttpGet]
		[Route("apiserver/v1/workouts")]
		[Produces("application/json")]
		public IActionResult GetWorkouts([FromQuery] bool limited, [FromQuery] long limit)
		{
			if (Request.Headers["STTAuthorization"] != _sessionKey)
			{
				return StatusCode(403);
			}
			var stream = new FileStream(@"Data\workouts_2.json", FileMode.Open, FileAccess.Read);
			return File(stream, "application/json");
		}
	}
}