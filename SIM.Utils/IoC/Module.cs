﻿using Microsoft.Extensions.DependencyInjection;
using SIM.Utils.Loaders;
using SIM.Utils.Savers;

namespace SIM.Utils.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection WithUtils(this IServiceCollection serviceCollection)
		{
			return serviceCollection
				.AddScoped<ILoaderData, SimpleJsonLoaderData>()
				.AddScoped<ISaverData, SimpleJsonSaverData>()
			;
		}
	}
}
