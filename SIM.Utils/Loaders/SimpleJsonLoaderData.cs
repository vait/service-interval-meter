using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SIM.Utils.Loaders
{
	public class SimpleJsonLoaderData : ILoaderData
	{
		/// <inheritdoc />
		public async ValueTask<T> LoadAsync<T>(string filePath, CancellationToken ct = default)
		{
			if (File.Exists(filePath))
			{
				await using var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				return await JsonSerializer.DeserializeAsync<T>(fs, (JsonSerializerOptions)null, ct).ConfigureAwait(false);
			}

			throw new FileNotFoundException(null, filePath);
		}
	}
}