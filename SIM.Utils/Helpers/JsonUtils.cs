﻿using System.Text.Json;

namespace SIM.Utils.Helpers
{
	public static class JsonUtils
	{
		public static JsonSerializerOptions CameCasePolice = new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.CamelCase};
	}
}
