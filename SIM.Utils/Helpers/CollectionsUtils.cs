using System.Collections.Generic;

namespace SIM.Utils.Helpers
{
	public static class ArrayUtils
	{
		public static bool IsEmpty<T>(this T[] array)
		{
			return array == null || array.Length == 0;
		}

		public static bool IsEmpty<T>(this HashSet<T> hash)
		{
			return hash == null || hash.Count == 0;
		}
	}
}