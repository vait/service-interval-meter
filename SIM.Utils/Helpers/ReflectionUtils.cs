using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;

namespace SIM.Utils.Helpers
{
	public static class ReflectionUtils
	{
		private static readonly ConcurrentDictionary<Type, PropertyInfo[]> _typeProperties = new ConcurrentDictionary<Type, PropertyInfo[]>();


		public static T GetAttribute<T>(this MemberInfo type, bool inherit = true)
			where T : Attribute
		{
			return (T)Attribute.GetCustomAttribute(type, typeof(T), inherit);
		}

		private static readonly ConcurrentDictionary<string, object> _typeAttributeMap =
			new ConcurrentDictionary<string, object>();


		public static T GetAttribute<T>(this Enum value) where T : Attribute
		{
			var type = value.GetType();
			var key = $"{type.FullName}.{typeof(T).FullName}.{value}";

			var result = _typeAttributeMap.GetOrAdd(key,
				k =>
				{
					var stringRepresentation = value.ToString();

					var field = type.GetField(stringRepresentation);
					if (field == null)
					{
						return stringRepresentation;
					}

					return field.GetAttribute<T>(false);
				});

			return (T)result;
		}

		public static IDictionary<string, string> ToDictionary<T>(this T obj) where T : class
		{
			var t = typeof(T);
			var properties = _typeProperties.GetOrAdd(t, t.GetProperties());
			var dict = new Dictionary<string, string>();
			foreach (var property in properties)
			{
				var val = property.GetValue(obj);
				if (val != null)
				{
					dict.Add(property.Name, property.GetValue(obj).ToString());
				}
			}

			return dict;
		}
	}
}
