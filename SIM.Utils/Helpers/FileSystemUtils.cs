using System;
using System.IO;

namespace SIM.Utils.Helpers
{
	internal static class FileSystemUtils
	{
		public static void CheckAndCreateDirectory(this string path)
		{
			var diretoryPath = Path.GetDirectoryName(path);
			if (!string.IsNullOrEmpty(diretoryPath))
			{
				var directory = new DirectoryInfo(diretoryPath);
				if (!directory.Exists)
				{
					directory.Create();
				}
			}
			else
			{
				throw new ArgumentNullException(nameof(diretoryPath));
			}
		}
	}
}
