using System.Threading;
using System.Threading.Tasks;

namespace SIM.Utils
{
	public interface ILoaderData
	{
		/// <summary>
		/// Загружает данные из нужного формата.
		/// </summary>
		ValueTask<T> LoadAsync<T>(string filePath, CancellationToken ct = default);
	}
}