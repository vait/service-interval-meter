using SIM.Utils.Helpers;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SIM.Utils.Savers
{
	public class SimpleJsonSaverData : ISaverData
	{
		private readonly JsonSerializerOptions options = new JsonSerializerOptions
		{
			WriteIndented = true,
			//IgnoreNullValues = true,
			PropertyNameCaseInsensitive = true
		};

		public async Task SaveAsync<T>(T data, string filePath, CancellationToken ct = default)
		{
			filePath.CheckAndCreateDirectory();
			await using var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
			await JsonSerializer.SerializeAsync(fs, data, options, ct).ConfigureAwait(false);
		}
	}
}
