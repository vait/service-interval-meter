namespace SIM.Utils.Models
{
	/// <summary>
	/// Обертка над запросом.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ActionResult<T>
	{
		public static readonly ActionResult<T> UnSuccess = new ActionResult<T>();

		public ActionResult()
		{
			IsSuccess = false;
			Data = default;
		}

		public ActionResult(T data)
		{
			Data = data;
			IsSuccess = true;
		}



		/// <summary>
		/// Успешно ли завершилась операция.
		/// </summary>
		public bool IsSuccess { get; }

		/// <summary>
		/// Данные.
		/// </summary>
		public T Data { get; }


	}
}