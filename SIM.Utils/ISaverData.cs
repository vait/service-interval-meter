using System.Threading;
using System.Threading.Tasks;

namespace SIM.Utils
{
	public interface ISaverData
	{
		/// <summary>
		/// Сохраняте данные в нужный формат.
		/// </summary>
		Task SaveAsync<T>(T data, string filePath, CancellationToken ct = default);
	}
}