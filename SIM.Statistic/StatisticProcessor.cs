using System.Collections.Concurrent;
using System.Threading.Tasks;
using SIM.Core.Models;
using SIM.Statistic.Helpers;
using SIM.Statistic.Models;

namespace SIM.Statistic
{
	/// <inheritdoc />
	public class StatisticProcessor : IMovesStatistic
	{
		public ServiceIntervalStatistic[] GetStatistic(Move[] movies, Equipment[] equipments)
		{
			var statistic = new ConcurrentQueue<ServiceIntervalStatistic>();
			Parallel.ForEach(equipments,
				eq =>
				{
					var eqStat = new ServiceIntervalStatistic
					{
						Equipment = eq
					};

					foreach (var move in movies)
					{
						if (eq.CanCountMove(move))
						{
							eqStat.Distance += move.Distance;
							eqStat.Duration += move.Duration;
							++eqStat.MovesCount;
						}
					}
					statistic.Enqueue(eqStat);

				});

			return statistic.ToArray();
		}
	}
}