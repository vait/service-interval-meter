using SIM.Core.Models;
using SIM.Utils.Helpers;
using System;
using System.Linq;

namespace SIM.Statistic.Helpers
{
	internal static class EquipmentHelper
	{
		public static bool CanCountMove(this Equipment equipment, Move move)
		{
			if (move.ActivityType != equipment.ActivityType)
			{
				return false;
			}

			if (move.StartDate < equipment.LastServiceDate)
			{
				return false;
			}

			if (equipment.Tags.IsEmpty() && move.Tags.IsEmpty())
			{
				return true;
			}

			return equipment.Tags.All(x => Enumerable.Contains(move.Tags, x, StringComparer.InvariantCultureIgnoreCase));
		}
	}
}
