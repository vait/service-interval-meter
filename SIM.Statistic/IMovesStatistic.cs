using SIM.Core.Models;
using SIM.Statistic.Models;

namespace SIM.Statistic
{
	/// <summary>
	/// Предоставляет методы анализа и сбора статистики по всем перемещениям.
	/// </summary>
	public interface IMovesStatistic
	{
		ServiceIntervalStatistic[] GetStatistic(Move[] movies, Equipment[] equipments);
	}
}