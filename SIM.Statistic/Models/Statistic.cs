using System;
using SIM.Core.Models;

namespace SIM.Statistic.Models
{
	/// <summary>
	/// Статистика межсервисного интервала.
	/// </summary>
	public class ServiceIntervalStatistic
	{
		/// <summary>
		/// Оборудование.
		/// </summary>
		public Equipment Equipment { get; set; }

		/// <summary>
		/// Пройденное расстояние.
		/// </summary>
		public long Distance { get; set; }

		/// <summary>
		/// Наработаное время.
		/// </summary>
		public TimeSpan Duration { get; set; }

		/// <summary>
		/// Количество перемещений.
		/// </summary>
		public long MovesCount { get; set; }
	}
}
