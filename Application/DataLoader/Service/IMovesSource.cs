﻿using DataLoader.Models;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface IMovesSource
	{
		Task<Move[]> GetDataAsync(string token, CancellationToken ct);

		Task<string> GetTokenAsync(string captcha, CancellationToken ct);
	}
}
