﻿using DataLoader.Models.Dto;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface ITagService
	{
		Task<TagDto> GetOrCreateTagByNameAsync(string tagNme, CancellationToken ct);
	}
}
