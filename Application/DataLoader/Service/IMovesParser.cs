using DataLoader.Models;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	public interface IMovesParser
	{
		/// <summary>
		/// Разбирает JSON.
		/// </summary>
		/// <param name="stream">Поток с JSON.</param>
		/// <param name="ct">CancellationToken.</param>
		/// <returns>Коллекция перемещения.</returns>
		ValueTask<Move[]> GetMoviesAsync(Stream stream, CancellationToken ct);
	}
}