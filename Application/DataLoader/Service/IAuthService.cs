﻿using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface IAuthService
	{
		Task<string> GetAsync(bool reload, CancellationToken ct);
	}
}
