﻿using DataLoader.Models;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface IActivityService
	{
		Task AddOrUpdateAsync(Move activity, CancellationToken ct);
	}
}
