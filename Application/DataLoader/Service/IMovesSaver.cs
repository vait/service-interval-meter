﻿using DataLoader.Models;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface IMovesSaver
	{
		Task SaveMovesAsync(Move[] moves, CancellationToken ct);
	}
}
