﻿using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service
{
	internal interface IStartable
	{
		Task StartAsync(CancellationToken ct);
	}
}
