﻿using DataLoader.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace DataLoader.Service.Implementation
{
	internal class MovesSaver : IMovesSaver
	{
		private readonly ActionBlock<Move> _saver;
		private readonly ILogger<MovesSaver> _logger;
		private readonly CancellationTokenSource _cancellationTokenSource;

		public MovesSaver(
			ILogger<MovesSaver> logger
			)
		{
			_logger = logger;

			_saver = new ActionBlock<Move>(SaveMoveAsync,
				new ExecutionDataflowBlockOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount * 2,
				});
		}

		public Task SaveMovesAsync(Move[] moves, CancellationToken ct)
		{
			foreach (Move move in moves)
			{
				if (!_saver.Post(move))
				{
					_logger.LogWarning($"Can't process object {JsonSerializer.Serialize(move)}");
				}
			}

			return _saver.Completion;
		}

		private async Task SaveMoveAsync(Move move)
		{
			try
			{

			}
			catch (Exception ex)
			{
				_logger.LogError(ex, nameof(SaveMoveAsync));
			}
		}
	}
}
