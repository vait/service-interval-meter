﻿using DataLoader.Infrastructure.Models;
using DataLoader.Models;
using DataLoader.Models.Response;
using Microsoft.Net.Http.Headers;
using SIM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using CacheControlHeaderValue = System.Net.Http.Headers.CacheControlHeaderValue;

namespace DataLoader.Service.Implementation.MovesSource
{
	public class SportsTrackerLoader : IMovesSource
	{
		private readonly IHttpClientFactory _httpClientFactory;
		private readonly ISIMConfigurationService _configuration;
		private readonly IMovesParser _movesParser;
		private readonly JsonSerializerOptions _serializerOptions;

		public SportsTrackerLoader(
			IHttpClientFactory httpClientFactory,
			ISIMConfigurationService configuration,
			IMovesParser movesParser)
		{
			_httpClientFactory = httpClientFactory;
			_configuration = configuration;
			_serializerOptions = new JsonSerializerOptions();
			_movesParser = movesParser;
		}

		public async Task<Move[]> GetDataAsync(string token, CancellationToken ct)
		{
			var stCfg = _configuration.Get<SportsTrackerConfiguration>(Constants.Configuration.SportsTrackerSection);
			using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, new Uri(new Uri(stCfg.Root), SportsTrackerConstants.Uri.Workouts)))
			{
				requestMessage.AddRequestHeaders();
				requestMessage.Headers.Add("STTAuthorization", token);

				using (var client = _httpClientFactory.CreateClient(HttpClientUtils.Name))
				{
					using (var response = await client.SendAsync(requestMessage, ct).ConfigureAwait(false))
					{
						if (response.IsSuccessStatusCode)
						{
							return await _movesParser.GetMoviesAsync(response.Content.ReadAsStream(ct), ct);
						}

						SportsTrackerResponse<EmptyPayload> errContent = null;
						try
						{
							errContent = await response.Content.ReadFromJsonAsync<SportsTrackerResponse<EmptyPayload>>().ConfigureAwait(false);
						}
						catch { }

						if (errContent != null)
						{

						}

						var errorMessage = $"[{SportsTrackerConstants.Uri.UserAuthorize}] {(int)response.StatusCode} ({response.ReasonPhrase}): {errContent?.Error?.Code} - {errContent?.Error?.Description}";

						throw new HttpRequestException(errorMessage, null, statusCode: response.StatusCode);
					}
				}

			}
		}

		public async Task<string> GetTokenAsync(string captcha, CancellationToken ct)
		{
			var stCfg = _configuration.Get<SportsTrackerConfiguration>(Constants.Configuration.SportsTrackerSection);

			using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(new Uri(stCfg.Root), SportsTrackerConstants.Uri.UserAuthorize)))
			{
				requestMessage.Content = new FormUrlEncodedContent(new[] {
					new KeyValuePair<string, string>("l", stCfg.Login),
					new KeyValuePair<string, string>("p", stCfg.Password),
					new KeyValuePair<string, string>("captchaToken", captcha),

				});
				requestMessage.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(SportsTrackerConstants.ContentType.UrlFormEncoded);

				requestMessage.AddRequestHeaders();

				using (var client = _httpClientFactory.CreateClient(HttpClientUtils.Name))
				{
					using (var response = await client.SendAsync(requestMessage, ct).ConfigureAwait(false))
					{
						if (response.IsSuccessStatusCode)
						{
							var authResult = await response.Content.ReadFromJsonAsync<SportsTrackerAuthResponse>(_serializerOptions, ct);

							if (authResult.Error == null)
							{
								return authResult.SessionKey;
							}

							throw new HttpRequestException($"Auth error: {authResult?.Error?.Code} - {authResult?.Error?.Description}");
						}

						SportsTrackerResponse<EmptyPayload> errContent = null;
						try
						{
							errContent = await response.Content.ReadFromJsonAsync<SportsTrackerResponse<EmptyPayload>>().ConfigureAwait(false);
						}
						catch { }

						var errorMessage = $"[{SportsTrackerConstants.Uri.UserAuthorize}] {(int)response.StatusCode} ({response.ReasonPhrase}): {errContent?.Error?.Code} - {errContent?.Error?.Description}";

						throw new HttpRequestException(errorMessage);
					}
				}
			}
		}



		/*private async Task ExecuteRequestAsync(Uri uri,
			HttpMethod method,
			HttpContent httpContent,
			CancellationToken ct,
			IReadOnlyDictionary<string, string> headers = null,
			Func<Stream, CancellationToken, Task> callBack = null)
		{
			using (var requestMessage = new HttpRequestMessage(method, uri))
			{
				if (httpContent != null)
				{
					requestMessage.Content = httpContent;
				}

				if (headers != null)
				{
					foreach (var header in headers)
					{
						requestMessage.Headers.Add(header.Key, header.Value);
					}
				}

				using (var response = await HttpClient.SendAsync(requestMessage, ct)
					.ConfigureAwait(false))
				{
					if (response.IsSuccessStatusCode)
					{
						if (callBack != null)
						{
							var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
							await callBack(stream, ct).ConfigureAwait(false);
						}

						return;
					}

					var errContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					var errorMessage = $"[{uri}] {(int)response.StatusCode} ({response.ReasonPhrase}): {errContent}";

					throw new HttpRequestException(errorMessage);
				}
			}
		}


			//https://api.sports-tracker.com/apiserver/v1/workouts?limited=true&limit=1000000
			//https://api.sports-tracker.com/apiserver/v1/user/feed/combined?limit=10&offset=0

protected override async Task<Move[]> LoadInternalAsync(DateTimeOffset? startPeriod, DateTimeOffset? endPeriod, CancellationToken ct)
{
	var moves = Array.Empty<Move>();

	var headers = new Dictionary<string, string>
	{
		{"STTAuthorization", _authKey}
	};

	await ExecuteRequestAsync(
	Constants.Uri.SportTracker.Workouts,
	HttpMethod.Get,
	null,
	ct,
	headers,
	async (stream, innerCt) =>
	{
		using (var reader = new StreamReader(stream))
		{
			moves = await Parser.GetMoviesAsync(reader, startPeriod, endPeriod, innerCt).ConfigureAwait(false);
		}
	}).ConfigureAwait(false);

	return moves;
}
*/

	}

	public static class Helpers
	{
		public static HttpRequestMessage AddRequestHeaders(this HttpRequestMessage request)
		{
			request.Headers.Add("DNT", "1");
			request.Headers.Add(HeaderNames.Pragma, "no-cache");
			request.Headers.CacheControl = new CacheControlHeaderValue { NoCache = true };
			request.Headers.Add(HeaderNames.Accept, "*/*");

			return request;
		}
	}
}