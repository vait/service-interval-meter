using DataLoader.Models;
using DataLoader.Models.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service.Implementation.MovesSource
{
	/// <inheritdoc />
	public class SportTrackerParser : IMovesParser
	{
		private readonly IReadOnlyDictionary<int, ActivityType> _map = new
			Dictionary<int, ActivityType>
		{
			{0, ActivityType.Walking},
			 {1, ActivityType.Running},
			 {2, ActivityType.Cycling},
			 {10, ActivityType.MountainBiking},
			 {11, ActivityType.Hiking},
			 {12, ActivityType.RollerSkating},
			 {13, ActivityType.AlpineSkiing},
			 {15, ActivityType.Rowing},
			 {16, ActivityType.Golf},
			 {21, ActivityType.PoolSwimming},
			 {22, ActivityType.TrailRunning},
			 {24, ActivityType.NordicWalking},
			 {25, ActivityType.HorsebackRiding},
			 {26, ActivityType.Motorsports},
			 {29, ActivityType.Climbing},
			 {30, ActivityType.Snowboarding},
			 {31, ActivityType.SkiTouring},
			 {34, ActivityType.Tennis},
			 {35, ActivityType.Basketball},
			 {36, ActivityType.Badminton},
			 {37, ActivityType.Baseball},
			 {38, ActivityType.Volleyball},
			 {39, ActivityType.AmericanFootball},
			 {40, ActivityType.TableTennis},
			 {41, ActivityType.RacquetBall},
			 {42, ActivityType.Squash},
			 {43, ActivityType.Floorball},
			 {44, ActivityType.Handball},
			 {45, ActivityType.Softball},
			 {46, ActivityType.Bowling},
			 {47, ActivityType.Cricket},
			 {48, ActivityType.Rugby},
			 {49, ActivityType.IceSkating},
			 {50, ActivityType.IceHockey},
			 {52, ActivityType.IndoorCycling},
			 {53, ActivityType.Treadmill},
			 {54, ActivityType.Crossfit},
			 {55, ActivityType.Crosstrainer},
			 {56, ActivityType.RollerSkiing},
			 {57, ActivityType.IndoorRowing},
			 {59, ActivityType.TrackAndField},
			 {60, ActivityType.Orienteering},
			 {61, ActivityType.StandupPaddling},
			 {62, ActivityType.MartialArts},
			 {63, ActivityType.Kettlebell},
			 {64, ActivityType.Dancing},
			 {65, ActivityType.SnowShoeing},
			 {66, ActivityType.Frisbee},
			 {68, ActivityType.Multisport},
			 {69, ActivityType.Aerobics},
			 {70, ActivityType.Trekking},
			 {71, ActivityType.Sailing},
			 {72, ActivityType.Kayaking},
			 {73, ActivityType.CircuitTraining},
			 {74, ActivityType.Triathlon},
			 {76, ActivityType.Cheerleading},
			 {77, ActivityType.Boxing},
			 {78, ActivityType.ScubaDiving},
			 {79, ActivityType.FreeDiving},
			 {80, ActivityType.AdventureRacing},
			 {81, ActivityType.Gymnastics},
			 {82, ActivityType.Canoeing},
			 {83, ActivityType.Mountaineering},
			 {84, ActivityType.TelemarkSkiing},
			 {85, ActivityType.OpenwaterSwimming},
			 {87, ActivityType.KitesurfingKiting},
			 {88, ActivityType.Paragliding},
			 {90, ActivityType.Snorkeling},
			 {91, ActivityType.Surfing},
			 {92, ActivityType.Swimrun},
			 {93, ActivityType.Duathlon},
			 {94, ActivityType.Aquathlon},
			 {95, ActivityType.ObstacleRacing},
			 {96, ActivityType.Fishing},
			 {97, ActivityType.Hunting},
			 {3, ActivityType.CrossCountrySkiing},
			 {14, ActivityType.Paddling},
			 {17, ActivityType.IndoorTraining},
			 {18, ActivityType.Parkour},
			 {19, ActivityType.BallGames},
			 {20, ActivityType.OutdoorGym},
			 {23, ActivityType.Gym},
			 {27, ActivityType.SkateBoarding},
			 {28, ActivityType.WaterSports},
			 {32, ActivityType.Fitness},
			 {33, ActivityType.SoccerFootball},
			 {51, ActivityType.YogaPilates},
			 {58, ActivityType.Stretching},
			 {67, ActivityType.Futsal},
			 {86, ActivityType.WindsurfingSurfing},
			 {4, ActivityType.UnspecifiedSport},
			 {5, ActivityType.UnspecifiedSport},
			 {6, ActivityType.UnspecifiedSport},
			 {7, ActivityType.UnspecifiedSport},
			 {8, ActivityType.UnspecifiedSport},
			 {9, ActivityType.UnspecifiedSport},
		};

		private readonly JsonSerializerOptions _serializerOptions;

		public SportTrackerParser()
		{
			_serializerOptions = new JsonSerializerOptions();
		}

		/// <inheritdoc />
		public async ValueTask<Move[]> GetMoviesAsync(Stream stream, CancellationToken ct)
		{
			var response = await JsonSerializer.DeserializeAsync<SportsTrackerResponse<Workout[]>>(stream, _serializerOptions, ct).ConfigureAwait(false);

			return response.Payload.Select(Convert).ToArray();
		}

		private Move Convert(Workout workout)
		{
			var activityType = ActivityType.UnspecifiedSport;
			if (_map.ContainsKey(workout.ActivityId))
			{
				activityType = _map[workout.ActivityId];
			}

			var tags = string.IsNullOrEmpty(workout.Description)
				? new HashSet<string>()
				: new HashSet<string>(workout.Description.Split(new[] { ' ', ',', '\n' }, StringSplitOptions.RemoveEmptyEntries));

			return new Move
			{
				Distance = (int)workout.TotalDistance,
				Tags = tags,
				WorkoutKey = workout.WorkoutKey,
				Duration = TimeSpan.FromSeconds(workout.TotalTime),
				StartDate = DateTimeOffset.FromUnixTimeMilliseconds(workout.StartTime).ToOffset(TimeSpan.FromMinutes(workout.TimeOffsetInMinutes)),
				ActivityType = activityType
			};
		}
	}
}
