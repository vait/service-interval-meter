﻿namespace DataLoader.Service.Implementation.MovesSource
{
	internal static class SportsTrackerConstants
	{
		internal static class ContentType
		{
			public const string Json = "application/json";
			public const string UrlFormEncoded = "application/x-www-form-urlencoded";
		}

		public static class Uri
		{
			public static string UserAuthorize = "apiserver/v1/login?source=javasc";
			public static string Workouts = "apiserver/v1/workouts?limited=true&limit=1000000";
		}
	}
}
