﻿using DataLoader.Models.Dto;
using DataLoader.Service.Implementation.Helpers;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service.Implementation
{
	internal class TagService : ITagService, IDisposable
	{
		private readonly ConcurrentDictionary<string, TagDto> _cahce;
		private readonly ITagDb _db;
		private readonly SemaphoreSlim _lock;

		public TagService(ITagDb db)
		{
			_db = db;
			_cahce = new ConcurrentDictionary<string, TagDto>();
			_lock = new SemaphoreSlim(1);
		}

		public async Task<TagDto> GetOrCreateTagByNameAsync(string tagNme, CancellationToken ct)
		{
			var name = tagNme.Trim().ToLower();
			if (!_cahce.TryGetValue(name, out var tag))
			{
				await _lock.WaitAsync();
				try
				{
					var tagDao = (await _db.FindAllAsync(x => x.Name.ToLower() == name, ct)).FirstOrDefault();

					if (tagDao == null)
					{
						tagDao = new TagDao
						{
							Name = name
						};

						tagDao = await _db.CreateAsync(tagDao, ct);
					}

					tag = _cahce.GetOrAdd(name, tagDao.MapToDto());
				}
				finally { _lock.Release(); }
			}

			return tag;
		}

		public void Dispose()
		{
			_lock?.Release();
			_lock?.Dispose();
		}


	}
}
