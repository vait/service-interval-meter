﻿using DataLoader.Models;
using DataLoader.Models.Dto;
using Repository.Models;

namespace DataLoader.Service.Implementation.Helpers
{
	internal static class DaoMappers
	{
		public static TagDto MapToDto(this TagDao dao)
		{
			TagDto entity = new TagDto
			{
				Id = dao.Id,
				Name = dao.Name
			};

			return entity;
		}

		public static ActivityDao MapToDao(this Move from)
		{
			ActivityDao entity = new ActivityDao
			{
				Distance = from.Distance,
				Duration = from.Duration.TotalSeconds,
				WorkoutKey = from.WorkoutKey,
				Start = from.StartDate.DateTime,
				Type = (ActivityTypeDao)from.ActivityType
			};

			return entity;
		}


		public static ActivityDao MapToDao(this Move from, ActivityDao activity)
		{
			activity.Distance = from.Distance;
			activity.Duration = from.Duration.TotalSeconds;
			activity.WorkoutKey = from.WorkoutKey;
			activity.Start = from.StartDate.DateTime;
			activity.Type = (ActivityTypeDao)from.ActivityType;

			return activity;
		}
	}
}
