﻿using DataLoader.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Retry;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service.Implementation
{
	internal class DataLoaderService : IDataLoaderService
	{
		private const string retryCountKey = "retrycount";
		private readonly IHostApplicationLifetime _appLifeTime;
		private readonly IAuthService _authService;
		private readonly IMovesSource _movesSource;
		private readonly IActivityService _activityService;
		private readonly AsyncRetryPolicy _policy;

		private Task _currentTask;
		public DataLoaderService(
			IConfiguration configuration,
			IHostApplicationLifetime appLifeTime,
			IAuthService authService,
			IMovesSource movesSource,
			IActivityService activityService)
		{
			_appLifeTime = appLifeTime;
			_authService = authService;
			_movesSource = movesSource;
			_activityService = activityService;

			_policy = Policy
				.Handle<HttpRequestException>(ex => ex.StatusCode == System.Net.HttpStatusCode.Unauthorized || ex.StatusCode == System.Net.HttpStatusCode.Forbidden)
				.WaitAndRetryAsync(new[] { TimeSpan.FromSeconds(10) }, (_, _, retryCount, context) =>
				{
					context[retryCountKey] = retryCount;
				});
		}

		public Task StartAsync(CancellationToken ct)
		{
			_currentTask = Execute(ct);

			return Task.CompletedTask;
		}

		private async Task Execute(CancellationToken ct)
		{
			var context = new Polly.Context
			{
				{retryCountKey, 0}
			};

			var loadedActivities = await _policy.ExecuteAsync(LoadData, context, ct);
			foreach (var activity in loadedActivities)
			{
				if (ct.IsCancellationRequested)
				{
					return;
				}

				await _activityService.AddOrUpdateAsync(activity, ct);

			}

			if (!ct.IsCancellationRequested)
			{
				_appLifeTime.StopApplication();
			}
		}

		private async Task<Move[]> LoadData(Context context, CancellationToken ct)
		{
			bool force = false;
			if (context.TryGetValue(retryCountKey, out var retryObject) && retryObject is int retries)
			{
				force = retries > 0;
			}

			var token = await _authService.GetAsync(force, ct);
			var result = await _movesSource.GetDataAsync(token, ct);

			return result;
		}
	}
}
