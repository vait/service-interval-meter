﻿using DataLoader.Infrastructure.Models;
using SIM.Infrastructure;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service.Implementation
{
	internal class AuthService : IAuthService
	{
		private readonly ISIMConfigurationService _configuration;
		private readonly IMovesSource _movesSource;

		public AuthService(
			ISIMConfigurationService configuration,
			IMovesSource movesSource)
		{
			_configuration = configuration;
			_movesSource = movesSource;
		}

		public async Task<string> GetAsync(bool reload, CancellationToken ct)
		{
			var cfg = _configuration.Get<DataLoaderConfiguration>(Constants.Configuration.DataLoaderSection);
			var authToken = string.Empty;

			//1 - строка с капчей
			//2 - строка с токеном
			var authLines = await File.ReadAllLinesAsync(Path.Combine("Data", cfg.TokenFileName));

			if (authLines.Length < 2 || reload)
			{
				return await CreateNewTokenAsync(authLines[0], ct);

			}
			else if (authLines.Length == 2)
			{
				return authLines[1];
			}
			else
			{
				//DB logger
				throw new UnauthorizedAccessException("Token not found");
			}
		}

		private async Task<string> CreateNewTokenAsync(string captcha, CancellationToken ct)
		{
			var cfg = _configuration.Get<DataLoaderConfiguration>(Constants.Configuration.DataLoaderSection);
			//есть капча, идем за токеном.
			var token = await _movesSource.GetTokenAsync(captcha, ct);

			using (var file = new StreamWriter(Path.Combine("Data", cfg.TokenFileName)))
			{
				await file.WriteLineAsync(captcha);
				await file.WriteLineAsync(token);
			}

			return token;
		}
	}
}
