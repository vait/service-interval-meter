﻿using DataLoader.Models;
using DataLoader.Service.Implementation.Helpers;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using Repository.Models;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Service.Implementation
{
	internal class ActivityService : IActivityService
	{
		private readonly IActivityDb _db;
		private readonly IActivityTagDb _tagLinkDb;
		private readonly ITagService _tagService;
		private readonly ILogger<ActivityService> _logger;

		public ActivityService(
			IActivityDb db,
			ITagService tagService,
			ILogger<ActivityService> logger,
			IActivityTagDb tagLinkDb)
		{
			_db = db;
			_tagService = tagService;
			_logger = logger;
			_tagLinkDb = tagLinkDb;
		}

		public async Task AddOrUpdateAsync(Move move, CancellationToken ct)
		{
			var activities = await _db.FindAllAsync(x => x.WorkoutKey == move.WorkoutKey, ct);
			if (activities.Length > 1)
			{
				_logger.LogError("Can't parse {0}: {1}", nameof(move.WorkoutKey), JsonSerializer.Serialize(move));
				return;
			}

			if (activities.Length == 0)
			{
				await CreateAsync(move, ct);
			}

			if (activities.Length == 1)
			{
				await UpdateAsync(move, activities[0], ct);
			}
		}

		private async Task CreateAsync(Move move, CancellationToken ct)
		{
			var newActivity = move.MapToDao();
			newActivity = await _db.CreateAsync(newActivity, ct).ConfigureAwait(false);
			var tagLink = new ActivityTagDao();

			foreach (var tag in move.Tags)
			{
				var dbTag = await _tagService.GetOrCreateTagByNameAsync(tag, ct).ConfigureAwait(false);
				tagLink.ActivityId = newActivity.Id;
				tagLink.TagId = dbTag.Id;

				await _tagLinkDb.CreateAsync(tagLink);
			}
		}


		private async Task UpdateAsync(Move move, ActivityDao activity, CancellationToken ct)
		{
			activity = move.MapToDao(activity);
			activity = await _db.UpdateAsync(activity, ct).ConfigureAwait(false);

			await _tagLinkDb.DeleteAsync(x => x.ActivityId == activity.Id, ct);

			var tagLink = new ActivityTagDao();

			foreach (var tag in move.Tags)
			{
				var dbTag = await _tagService.GetOrCreateTagByNameAsync(tag, ct).ConfigureAwait(false);
				tagLink.ActivityId = activity.Id;
				tagLink.TagId = dbTag.Id;

				await _tagLinkDb.CreateAsync(tagLink);
			}
		}

	}
}
