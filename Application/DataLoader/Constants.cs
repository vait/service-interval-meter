﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SportsTrackerMock")]
namespace DataLoader
{
	internal class Constants
	{
		public static class Configuration
		{
			public static string SportsTrackerSection = "SportsTracker";
			public static string DataLoaderSection = "DataLoader";
		}
	}
}
