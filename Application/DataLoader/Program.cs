﻿using DataLoader.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Repository.IoC;
using SIM.Infrastructure;
using SIM.Infrastructure.IoC;
using System.IO;

var builder = Host.CreateDefaultBuilder(args)
	.ConfigureAppConfiguration((hostingContext, config) =>
	{
		config
		.SetBasePath(Directory.GetCurrentDirectory())
		.AddJsonFile("appsettings.json", false, false)
		.AddJsonFile("appsettings." + hostingContext.HostingEnvironment.EnvironmentName + ".json", true, false);

		if (hostingContext.HostingEnvironment.IsDevelopment())
		{
			config
			.AddSharedJsonFile(hostingContext, 4);
		}

		config.AddCommandLine(args);
	})
	.ConfigureServices(services =>
	{
		services
			.ApplyLoaderDI()
			.UseSIMInfrastructure()
			.AddDefaultHttpClient()
			.UseDb()
		;
	});

var host = builder.Build();

await host.RunAsync().ConfigureAwait(false);