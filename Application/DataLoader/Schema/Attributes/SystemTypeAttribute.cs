using System;

namespace DataLoader.Schema.Attributes
{
	/// <summary>
	/// Системный тип свойства Move.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field)]
	internal class SystemTypeAttribute : Attribute
	{
		/// <summary>
		/// Тип.
		/// </summary>
		public Type Type { get; }

		public SystemTypeAttribute(Type systemType)
		{
			Type = systemType;
		}
	}
}