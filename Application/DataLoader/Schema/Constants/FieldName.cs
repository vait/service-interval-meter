namespace DataLoader.Schema.Constants
{
	internal static class FieldName
	{
		internal const string Year = "Year";
		internal const string Month = "Month";
		internal const string Day = "Day";
		internal const string Distance = "Distance";
		internal const string Duration = "Duration";
		internal const string Tags = "Tags";
		internal const string ActivityId = "ActivityID";
		internal const string MoveId = "MoveID";
	}
}