using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DataLoader.Models
{
	/// <summary>
	/// Перемещение.
	/// </summary>
	[DebuggerDisplay("[{ActivityType,G}] {StartDate,d}")]
	public class Move : IComparable<Move>
	{
		/// <summary>
		/// Идентификатор перемещения.
		/// </summary>
		public string WorkoutKey { get; set; }

		/// <summary>
		/// Вид спорта.
		/// </summary>
		public ActivityType ActivityType { get; set; }

		/// <summary>
		/// Дата начала
		/// </summary>
		public DateTimeOffset StartDate { get; set; }

		/// <summary>
		/// Длительность.
		/// </summary>
		public TimeSpan Duration { get; set; }

		/// <summary>
		/// Пройденное расстояние.
		/// </summary>
		public int Distance { get; set; }

		/// <summary>
		/// Тэги. Используются для фильтрации.
		/// </summary>
		public HashSet<string> Tags { get; set; }

		public Move()
		{
			Tags = new HashSet<string>();
		}

		public int CompareTo(Move otherMove)
		{
			if (StartDate != otherMove.StartDate)
			{
				return StartDate.CompareTo(otherMove.StartDate);
			}

			return WorkoutKey.CompareTo(otherMove.WorkoutKey);
		}
	}
}