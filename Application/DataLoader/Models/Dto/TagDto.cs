﻿namespace DataLoader.Models.Dto
{
	internal record class TagDto
	{
		public long Id { get; set; }
		public string Name { get; set; }
	}
}
