﻿using System.Text.Json.Serialization;

namespace DataLoader.Models.Response
{
	internal class SportsTrackerAuthResponse
	{
		[JsonPropertyName("username")]
		public string Username { get; set; }

		[JsonPropertyName("userKey")]
		public string UserKey { get; set; }

		[JsonPropertyName("uuid")]
		public string Uuid { get; set; }

		[JsonPropertyName("sessionkey")]
		public string SessionKey { get; set; }

		[JsonPropertyName("key")]
		public string Key { get; set; }

		[JsonPropertyName("error")]
		public Error Error { get; set; }
	}
}
