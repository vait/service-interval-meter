﻿using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

[assembly: InternalsVisibleTo("TestApp")]

namespace DataLoader.Models.Response
{
	internal class Workout
	{
		/// <summary>
		/// Вид спорта.
		/// </summary>
		[JsonPropertyName("activityId")]
		public int ActivityId { get; set; }

		[JsonPropertyName("description")]
		public string Description { get; set; }

		[JsonPropertyName("startTime")]
		public long StartTime { get; set; }

		[JsonPropertyName("totalTime")]
		public double TotalTime { get; set; }

		[JsonPropertyName("totalDistance")]
		public double TotalDistance { get; set; }

		/// <summary>
		/// Идентификатор занятия.
		/// </summary>
		[JsonPropertyName("workoutKey")]
		public string WorkoutKey { get; set; }

		[JsonPropertyName("timeOffsetInMinutes")]
		public int TimeOffsetInMinutes { get; set; }
	}
}
