﻿using System.Text.Json.Serialization;

namespace DataLoader.Models.Response
{
	internal class Error
	{
		[JsonPropertyName("code")]
		public string Code { get; set; }

		[JsonPropertyName("description")]
		public string Description { get; set; }
	}
}
