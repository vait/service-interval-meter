﻿using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

[assembly: InternalsVisibleTo("SportsTrackerMock")]

namespace DataLoader.Models.Response
{
	internal class SportsTrackerResponse<T>
	{
		[JsonPropertyName("payload")]
		public T Payload { get; set; }

		[JsonPropertyName("error")]
		public Error Error { get; set; }
	}
}
