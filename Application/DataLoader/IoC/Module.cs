﻿using DataLoader.Infrastructure;
using DataLoader.Service;
using DataLoader.Service.Implementation;
using DataLoader.Service.Implementation.MovesSource;
using Microsoft.Extensions.DependencyInjection;

namespace DataLoader.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection ApplyLoaderDI(this IServiceCollection serviceCollection)
		{
			return
				serviceCollection
					.AddHostedService<AppService>()
					.AddSingleton<IDataLoaderService, DataLoaderService>()
					.AddSingleton<IAuthService, AuthService>()
					.AddSingleton<IMovesSource, SportsTrackerLoader>()
					.AddSingleton<IMovesParser, SportTrackerParser>()
					.AddSingleton<ITagService, TagService>()
					.AddSingleton<IActivityService, ActivityService>()
				;
		}
	}
}
