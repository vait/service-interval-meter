namespace DataLoader.Infrastructure.Models
{
	/// <summary>
	/// Конфигурация приложения.
	/// </summary>
	public class SportsTrackerConfiguration
	{
		/// <summary>
		/// Путь к сайту.
		/// </summary>
		public string Root { get; set; } = string.Empty;

		/// <summary>
		/// Имя пользователя.
		/// </summary>
		public string Login { get; set; } = string.Empty;

		/// <summary>
		/// Пароль.
		/// </summary>
		public string Password { get; set; } = string.Empty;
	}
}
