﻿using DataLoader.Service;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DataLoader.Infrastructure
{
	internal class AppService : IHostedService, IDisposable
	{
		private readonly IDataLoaderService _dataLoaderService;
		private CancellationTokenSource? _cts;

		public AppService(IDataLoaderService dataLoaderService)
		{
			_dataLoaderService = dataLoaderService;
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			_cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
			return _dataLoaderService.StartAsync(_cts.Token);
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_cts?.Cancel();
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_cts?.Dispose();
		}
	}
}
