using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SIM.Application.Core.Configuration;
using SIM.Core.IoC;
using SIM.UI.Controllers;
using SIM.UI.IoC;
using SIM.UI.Presenters;
using SIM.Utils.IoC;
using System;
using System.Windows.Forms;
using WinFormsApplication = System.Windows.Forms.Application;

namespace SIM.UI
{
	internal static class Program
	{
		static string PublicConfigFile;
		static string PrivateConfigFile;

		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			WinFormsApplication.SetHighDpiMode(HighDpiMode.SystemAware);
			WinFormsApplication.EnableVisualStyles();
			WinFormsApplication.SetCompatibleTextRenderingDefault(false);

			var configFileName = "appsettings.json";
			PublicConfigFile = System.IO.Path.Combine(WinFormsApplication.StartupPath, configFileName);
			PrivateConfigFile = System.IO.Path.Combine(WinFormsApplication.LocalUserAppDataPath, configFileName);

			var builder = new HostBuilder()
				.ConfigureAppConfiguration((hostContext, config) =>
				{
					config
						.AddJsonFile(PublicConfigFile, optional: true)
						.AddJsonFile(PrivateConfigFile, optional: true)
						.AddCustomCommandLine(args);
				})
				.ConfigureServices((hostContext, services) =>
				{
					services.WithUtils();
					services.WithCore();
					services.WithUI();
					services.AddSingleton(serviceProvider =>
					{
						return new ApplicationController(serviceProvider, PrivateConfigFile, PublicConfigFile);
					});
				});

			var host = builder.Build();

			using (var serviceScope = host.Services.CreateScope())
			{
				var container = serviceScope.ServiceProvider;

				var controller = container.GetRequiredService<ApplicationController>();

				controller.Run<IMainPresenter>();
			}
		}
	}
}



//https://andrewlock.net/using-dependency-injection-in-a-net-core-console-application/
//https://thecodebuzz.com/dependency-injection-net-core-windows-form-generic-hostbuilder/
//https://csharp.christiannagel.com/2016/08/16/diwithconfiguration/
//https://habr.com/ru/post/502358/ -Реализация MVP на основе ApplicationController и IoC в WinForms приложении
