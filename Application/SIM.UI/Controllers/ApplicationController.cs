﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SIM.Application.Core.Dto;
using SIM.UI.Presenters;
using SIM.UI.Views;
using System;
using System.Threading.Tasks;

namespace SIM.UI.Controllers
{
	public class ApplicationController
	{
		private readonly IServiceProvider _container;
		public Configuration Config { get; }

		public string PrivateConfigFile { get; }

		public string PublicConfigFile { get; }

		public ApplicationController(IServiceProvider container, string privateConfigFile, string publicConfigFile)
		{
			_container = container;
			var cfg = _container.GetService<IConfiguration>();
			Config = cfg.Get<Configuration>();
			PrivateConfigFile = privateConfigFile;
			PublicConfigFile = publicConfigFile;
		}

		public void Run<TPresenter>() where TPresenter : class, IPresenter
		{
			var presenter = _container.GetRequiredService<TPresenter>();
			presenter.Run();
		}

		public void Run<TPresenter, TArgs>(TArgs args) where TPresenter : class, IPresenter<TArgs>
		{
			var presenter = _container.GetRequiredService<TPresenter>();
			presenter.Run(args);
		}

		public void RunLoader(Func<Action<string>, Task> loader)
		{
			var presenter = _container.GetRequiredService<ILoaderPresenter>();
			presenter.Run(loader);
		}

		public TView ResolveView<TView>() where TView : class, IFormView
		{
			return _container.GetRequiredService<TView>();
		}
	}
}
