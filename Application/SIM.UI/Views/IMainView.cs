﻿using System;
using System.Threading.Tasks;

namespace SIM.UI.Views
{
	public interface IMainView : IFormView
	{
		event Action ChangeService;
		event Action ChangeSettings;
		event Action<Func<Action<string>, Task>> Test;
	}
}
