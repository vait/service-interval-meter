﻿using SIM.UI.Models;

namespace SIM.UI.Views
{
	public interface ISettingsView : IEditView<SettingsUiObject>
	{
	}
}
