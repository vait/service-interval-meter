﻿using SIM.UI.Models;
using System;
using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
	public partial class SettingsView : EditView<SettingsUiObject>, ISettingsView
	{
		public DateTime StartDate
		{
			get => ValidForPickerStartDate;

			set
			{
				Args.StartDate = value;
			}
		}

		public DateTime LastDate
		{
			get => ValidForPickerEndDate;

			set
			{
				Args.LastDate = value;
			}
		}

		public bool UseStartDate
		{
			get
			{
				return Args.StartDate == null;
			}

			set
			{
				if (!value)
				{
					Args.StartDate = startDate.Value;
				}
				else
				{
					startDate.Value = ValidForPickerStartDate;
					Args.StartDate = DateTime.MinValue;
				}
			}
		}

		public bool UseLastDate
		{
			get
			{
				return Args.LastDate == null;
			}

			set
			{
				if (!value)
				{
					Args.LastDate = lastDate.Value;
				}
				else
				{
					startDate.Value = ValidForPickerEndDate;
					Args.LastDate = null;
				}
			}
		}

		public SettingsView(ApplicationContext context) : base(context)
		//public SettingsView()
		{
			InitializeComponent();
		}

		protected override void SetDataBindings()
		{
			login.DataBindings.Add(nameof(login.Text), Args, nameof(Args.Login));
			password.DataBindings.Add(nameof(password.Text), Args, nameof(Args.Password));
			stLogin.DataBindings.Add(nameof(stLogin.Text), Args, nameof(Args.StLogin));
			stPassword.DataBindings.Add(nameof(stPassword.Text), Args, nameof(Args.StPassword));
			moves.DataBindings.Add(nameof(moves.Text), Args, nameof(Args.Moves));
			equipments.DataBindings.Add(nameof(equipments.Text), Args, nameof(Args.Equipments));

			startDate.DataBindings.Add(nameof(startDate.Value), this, nameof(StartDate));
			fromInfinity.DataBindings.Add(nameof(fromInfinity.Checked), this, nameof(UseStartDate));

			lastDate.DataBindings.Add(nameof(lastDate.Value), this, nameof(LastDate));
			toInfinity.DataBindings.Add(nameof(toInfinity.Checked), this, nameof(UseLastDate));

			startDate.Enabled = !UseStartDate;
			lastDate.Enabled = !UseLastDate;
		}

		private DateTime ValidForPickerStartDate
		{
			get
			{
				var date = Args.StartDate;
				if (Args.StartDate < startDate.MinDate)
				{
					date = DateTime.Now.Date;
				}

				return date.Value.Date;
			}
		}

		private DateTime ValidForPickerEndDate
		{
			get
			{
				var date = Args.LastDate;
				if (Args.LastDate == null)
				{
					date = DateTime.Now.Date;
				}

				return date.Value.Date;
			}
		}

		private void FromInfinity_CheckedChanged(object sender, EventArgs e)
		{
			startDate.Enabled = !fromInfinity.Checked;
		}

		private void ToInfinity_CheckedChanged(object sender, EventArgs e)
		{
			lastDate.Enabled = !toInfinity.Checked;
		}
	}
}