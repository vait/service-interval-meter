﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
	public class BaseView : Form, IView
	{
		protected readonly MethodInvoker CloseInvoker;
		protected readonly ApplicationContext Context;

		public event Action OnClose;
		public event Func<Task> OnLoadedAsync;

		public BaseView(ApplicationContext context)
		{
			Context = context;
			CloseInvoker = Close;
			FormClosing += OnFormClosing;
		}

		public virtual void ShowView()
		{
			Show();
		}

		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			var closeHandlers = OnClose;
			if (closeHandlers != null)
			{
				Invoke(closeHandlers);
			}
		}

		protected override async void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			var onLoadHandlers = OnLoadedAsync;
			if (onLoadHandlers != null)
			{
				await onLoadHandlers().ConfigureAwait(false);
			}
		}
	}
}
