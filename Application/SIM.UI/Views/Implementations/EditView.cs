﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
	public abstract class EditView<TArgs> : ArgumentedView<TArgs>, IEditView<TArgs> where TArgs : class, INotifyPropertyChanged
	{
		public event Func<TArgs, Task> Save;
		public event Func<Task> Cancel;

		protected EditView(ApplicationContext context) : base(context)
		{
		}

		protected async void Cancel_Click(object sender, EventArgs e)
		{
			var cancelSubscribers = Cancel;
			cancelSubscribers?.Invoke();

			var handler = Cancel;

			if (handler == null)
			{
				return;
			}

			var invocationList = handler.GetInvocationList();
			var handlerTasks = new Task[invocationList.Length];

			for (var i = 0; i < invocationList.Length; i++)
			{
				handlerTasks[i] = ((Func<Task>)invocationList[i])();
			}

			await Task.WhenAll(handlerTasks);
		}

		protected async void Save_Click(object sender, EventArgs e)
		{
			var handler = Save;

			if (handler == null)
			{
				return;
			}

			var invocationList = handler.GetInvocationList();
			var handlerTasks = new Task[invocationList.Length];

			for (var i = 0; i < invocationList.Length; i++)
			{
				handlerTasks[i] = ((Func<TArgs, Task>)invocationList[i])(Args);
			}

			await Task.WhenAll(handlerTasks);
		}

	}
}
