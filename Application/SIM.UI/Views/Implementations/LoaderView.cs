﻿using System.Windows.Forms;
using WinFormsApplication = System.Windows.Forms.Application;

namespace SIM.UI.Views.Implementations
{
	public partial class LoaderView : BaseView, ILoaderView
	{
		public LoaderView(ApplicationContext context) : base(context)
		{
			InitializeComponent();
			caption.Text = WinFormsApplication.ProductName;
		}

		public override void ShowView()
		{
			ShowDialog();
		}

		public void SafeClose(){
			Invoke(CloseInvoker);
		}

		public void ShowInformation(string text)
		{
			Invoke((MethodInvoker)(() => information.Text = text));
		}
	}
}
