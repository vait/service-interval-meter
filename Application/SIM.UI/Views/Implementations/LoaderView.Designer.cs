﻿using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
    partial class LoaderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.caption = new System.Windows.Forms.Label();
            this.information = new System.Windows.Forms.Label();
            this.SuspendLayout();
            //
            // progressBar1
            //
            this.progressBar1.Location = new System.Drawing.Point(12, 59);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(464, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 0;
            this.progressBar1.UseWaitCursor = true;
            //
            // caption
            //
            this.caption.AutoSize = true;
            this.caption.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.caption.Location = new System.Drawing.Point(12, 9);
            this.caption.Name = "caption";
            this.caption.Size = new System.Drawing.Size(144, 21);
            this.caption.TabIndex = 1;
            this.caption.Text = "WinFormsSurface";
            this.caption.UseWaitCursor = true;
            //
            // information
            //
            this.information.Location = new System.Drawing.Point(12, 109);
            this.information.Name = "information";
            this.information.Size = new System.Drawing.Size(464, 46);
            this.information.TabIndex = 2;
            //
            // Loader
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 164);
            this.Controls.Add(this.information);
            this.Controls.Add(this.caption);
            this.Controls.Add(this.progressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoaderView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loader";
            this.UseWaitCursor = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label caption;
        private Label information;
    }
}
