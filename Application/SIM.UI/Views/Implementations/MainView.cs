﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsApplication = System.Windows.Forms.Application;

namespace SIM.UI.Views.Implementations
{
	public partial class MainView : BaseView, IMainView
	{
		public event Action ChangeService;
		public event Action ChangeSettings;
		public event Action<Func<Action<string>, Task>> Test;

		//public MainView()
		public MainView(ApplicationContext context) : base(context)
		{
			InitializeComponent();
		}

		public override void ShowView()
		{
			Context.MainForm = this;
			WinFormsApplication.Run(Context);
		}

		private void toolStripMenuItem5_Click(object sender, EventArgs e)
		{
			var сhangeServiceSubscribers = ChangeService;
			сhangeServiceSubscribers?.Invoke();
		}

		private void Options_Click(object sender, EventArgs e)
		{
			var сhangeSettingsSubscribers = ChangeSettings;
			сhangeSettingsSubscribers?.Invoke();


			/*	var сhangeServiceSubscribers = Test;
						сhangeServiceSubscribers?.Invoke(async shower =>
						{
							//await Task.Delay(5000).ConfigureAwait(false);
							shower("ssdsfd");
							//await Task.Delay(5000).ConfigureAwait(false);
							//showInfo("456");
							await Task.Delay(5000).ConfigureAwait(false);
						});*/
		}
	}
}
