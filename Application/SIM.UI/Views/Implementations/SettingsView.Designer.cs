﻿using System.ComponentModel;

namespace SIM.UI.Views.Implementations
{
	partial class SettingsView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}

			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.password = new System.Windows.Forms.TextBox();
			this.login = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.moves = new System.Windows.Forms.TextBox();
			this.equipments = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.toInfinity = new System.Windows.Forms.CheckBox();
			this.fromInfinity = new System.Windows.Forms.CheckBox();
			this.lastDate = new System.Windows.Forms.DateTimePicker();
			this.startDate = new System.Windows.Forms.DateTimePicker();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.save = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.stPassword = new System.Windows.Forms.TextBox();
			this.stLogin = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.SuspendLayout();
			//
			// groupBox1
			//
			this.groupBox1.Controls.Add(this.password);
			this.groupBox1.Controls.Add(this.login);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(14, 14);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox1.Size = new System.Drawing.Size(376, 120);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "movescount.com";
			//
			// password
			//
			this.password.Location = new System.Drawing.Point(8, 83);
			this.password.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.password.Name = "password";
			this.password.PasswordChar = '*';
			this.password.Size = new System.Drawing.Size(361, 23);
			this.password.TabIndex = 2;
			//
			// login
			//
			this.login.Location = new System.Drawing.Point(8, 37);
			this.login.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.login.Name = "login";
			this.login.Size = new System.Drawing.Size(361, 23);
			this.login.TabIndex = 1;
			//
			// label2
			//
			this.label2.Location = new System.Drawing.Point(8, 65);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(91, 15);
			this.label2.TabIndex = 0;
			this.label2.Text = "Password";
			//
			// label1
			//
			this.label1.Location = new System.Drawing.Point(8, 19);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Login";
			//
			// groupBox2
			//
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.moves);
			this.groupBox2.Controls.Add(this.equipments);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Location = new System.Drawing.Point(14, 266);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox2.Size = new System.Drawing.Size(376, 118);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Offline settings";
			//
			// label4
			//
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(8, 63);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(81, 15);
			this.label4.TabIndex = 2;
			this.label4.Text = "Moves (JSON)";
			//
			// moves
			//
			this.moves.Location = new System.Drawing.Point(8, 81);
			this.moves.Name = "moves";
			this.moves.Size = new System.Drawing.Size(361, 23);
			this.moves.TabIndex = 8;
			//
			// equipments
			//
			this.equipments.Location = new System.Drawing.Point(8, 37);
			this.equipments.Name = "equipments";
			this.equipments.Size = new System.Drawing.Size(361, 23);
			this.equipments.TabIndex = 7;
			//
			// label3
			//
			this.label3.Location = new System.Drawing.Point(8, 19);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(132, 15);
			this.label3.TabIndex = 0;
			this.label3.Text = "Equipments (require)";
			//
			// groupBox3
			//
			this.groupBox3.Controls.Add(this.toInfinity);
			this.groupBox3.Controls.Add(this.fromInfinity);
			this.groupBox3.Controls.Add(this.lastDate);
			this.groupBox3.Controls.Add(this.startDate);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Location = new System.Drawing.Point(14, 390);
			this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox3.Size = new System.Drawing.Size(376, 100);
			this.groupBox3.TabIndex = 9;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Period of analysis";
			//
			// toInfinity
			//
			this.toInfinity.AutoSize = true;
			this.toInfinity.Location = new System.Drawing.Point(202, 66);
			this.toInfinity.Name = "toInfinity";
			this.toInfinity.Size = new System.Drawing.Size(108, 19);
			this.toInfinity.TabIndex = 13;
			this.toInfinity.Text = "[Start date; +∞)";
			this.toInfinity.UseVisualStyleBackColor = true;
			this.toInfinity.Click += new System.EventHandler(ToInfinity_CheckedChanged);
			//
			// fromInfinity
			//
			this.fromInfinity.AutoSize = true;
			this.fromInfinity.Location = new System.Drawing.Point(8, 66);
			this.fromInfinity.Name = "fromInfinity";
			this.fromInfinity.Size = new System.Drawing.Size(101, 19);
			this.fromInfinity.TabIndex = 11;
			this.fromInfinity.Text = "(-∞; End date]";
			this.fromInfinity.UseVisualStyleBackColor = true;
			this.fromInfinity.Click += new System.EventHandler(FromInfinity_CheckedChanged);
			//
			// lastDate
			//
			this.lastDate.Location = new System.Drawing.Point(202, 37);
			this.lastDate.Name = "lastDate";
			this.lastDate.Size = new System.Drawing.Size(167, 23);
			this.lastDate.TabIndex = 12;
			//
			// startDate
			//
			this.startDate.Location = new System.Drawing.Point(8, 37);
			this.startDate.Name = "startDate";
			this.startDate.Size = new System.Drawing.Size(167, 23);
			this.startDate.TabIndex = 10;
			//
			// label6
			//
			this.label6.Location = new System.Drawing.Point(202, 19);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(117, 15);
			this.label6.TabIndex = 0;
			this.label6.Text = "End date";
			//
			// label5
			//
			this.label5.Location = new System.Drawing.Point(8, 19);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(117, 15);
			this.label5.TabIndex = 0;
			this.label5.Text = "Start date";
			//
			// save
			//
			this.save.Location = new System.Drawing.Point(114, 496);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(75, 23);
			this.save.TabIndex = 14;
			this.save.Text = "Save";
			this.save.UseVisualStyleBackColor = true;
			this.save.Click += Save_Click;
			//
			// cancel
			//
			this.cancel.Location = new System.Drawing.Point(216, 496);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 15;
			this.cancel.Text = "Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			this.cancel.Click += Cancel_Click;
			//
			// stPassword
			//
			this.stPassword.Location = new System.Drawing.Point(8, 83);
			this.stPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.stPassword.Name = "stPassword";
			this.stPassword.PasswordChar = '*';
			this.stPassword.Size = new System.Drawing.Size(361, 23);
			this.stPassword.TabIndex = 5;
			//
			// stLogin
			//
			this.stLogin.Location = new System.Drawing.Point(8, 37);
			this.stLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.stLogin.Name = "stLogin";
			this.stLogin.Size = new System.Drawing.Size(361, 23);
			this.stLogin.TabIndex = 4;
			//
			// label7
			//
			this.label7.Location = new System.Drawing.Point(8, 65);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(91, 15);
			this.label7.TabIndex = 0;
			this.label7.Text = "Password";
			//
			// label8
			//
			this.label8.Location = new System.Drawing.Point(8, 19);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(76, 15);
			this.label8.TabIndex = 0;
			this.label8.Text = "Login";
			//
			// groupBox4
			//
			this.groupBox4.Controls.Add(this.stPassword);
			this.groupBox4.Controls.Add(this.stLogin);
			this.groupBox4.Controls.Add(this.label7);
			this.groupBox4.Controls.Add(this.label8);
			this.groupBox4.Location = new System.Drawing.Point(14, 140);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.groupBox4.Size = new System.Drawing.Size(376, 120);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "sport-tracker.com";
			//
			// SettingsView
			//
			this.AcceptButton = this.save;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(406, 534);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.save);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.MaximizeBox = false;
			this.Name = "SettingsView";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SettingsView";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.ResumeLayout(false);

		}

		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox login;
		private System.Windows.Forms.TextBox password;

		private System.Windows.Forms.GroupBox groupBox2;

		private System.Windows.Forms.GroupBox groupBox1;

		#endregion

		private System.Windows.Forms.TextBox equipments;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox moves;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.DateTimePicker startDate;
		private System.Windows.Forms.DateTimePicker lastDate;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox fromInfinity;
		private System.Windows.Forms.CheckBox toInfinity;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.TextBox stPassword;
		private System.Windows.Forms.TextBox stLogin;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox4;
	}
}
