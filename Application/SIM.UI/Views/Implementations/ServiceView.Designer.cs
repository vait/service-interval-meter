﻿namespace SIM.UI.Views.Implementations
{
    partial class ServiceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.dateService = new System.Windows.Forms.DateTimePicker();
			this.save = new System.Windows.Forms.Button();
			this.SuspendLayout();

			//
			// label1
			//
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(10, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(79, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date of service";

			//
			// dateService
			//
			this.dateService.Location = new System.Drawing.Point(10, 23);
			this.dateService.Name = "dateService";
			this.dateService.Size = new System.Drawing.Size(172, 20);
			this.dateService.TabIndex = 1;

			//
			// cancel
			//
			this.save.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.save.Location = new System.Drawing.Point(64, 59);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(64, 20);
			this.save.TabIndex = 3;
			this.save.Text = "OK";
			this.save.UseVisualStyleBackColor = true;
			this.save.Click += new System.EventHandler(this.SaveOnClick);

			//
			// ServiceView
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AcceptButton = this.save;
			this.ClientSize = new System.Drawing.Size(195, 91);
			this.Controls.Add(this.save);
			this.Controls.Add(this.dateService);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ServiceView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Service infomation";
			this.ResumeLayout(false);
			this.PerformLayout();
		}

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateService;
		private System.Windows.Forms.Button save;
    }
}
