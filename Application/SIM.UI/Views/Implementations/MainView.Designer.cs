﻿namespace SIM.UI.Views.Implementations
{
    partial class MainView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.equipments = new System.Windows.Forms.ListBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.summary = new System.Windows.Forms.TabPage();
			this.configuration = new System.Windows.Forms.TabPage();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.testChangeService = new System.Windows.Forms.ToolStripMenuItem();
			this.options = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			//
			// splitContainer1
			//
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.splitContainer1.Name = "splitContainer1";
			//
			// splitContainer1.Panel1
			//
			this.splitContainer1.Panel1.Controls.Add(this.equipments);
			//
			// splitContainer1.Panel2
			//
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Size = new System.Drawing.Size(800, 426);
			this.splitContainer1.SplitterDistance = 265;
			this.splitContainer1.TabIndex = 0;
			this.splitContainer1.Text = "splitContainer1";
			//
			// equipments
			//
			this.equipments.Dock = System.Windows.Forms.DockStyle.Fill;
			this.equipments.FormattingEnabled = true;
			this.equipments.ItemHeight = 15;
			this.equipments.Location = new System.Drawing.Point(0, 0);
			this.equipments.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.equipments.Name = "equipments";
			this.equipments.Size = new System.Drawing.Size(265, 426);
			this.equipments.TabIndex = 0;
			//
			// tabControl1
			//
			this.tabControl1.Controls.Add(this.summary);
			this.tabControl1.Controls.Add(this.configuration);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(531, 426);
			this.tabControl1.TabIndex = 0;
			//
			// summary
			//
			this.summary.Location = new System.Drawing.Point(4, 24);
			this.summary.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.summary.Name = "summary";
			this.summary.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.summary.Size = new System.Drawing.Size(523, 398);
			this.summary.TabIndex = 0;
			this.summary.Text = "Summary";
			this.summary.UseVisualStyleBackColor = true;
			//
			// configuration
			//
			this.configuration.Location = new System.Drawing.Point(4, 24);
			this.configuration.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.configuration.Name = "configuration";
			this.configuration.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.configuration.Size = new System.Drawing.Size(523, 394);
			this.configuration.TabIndex = 1;
			this.configuration.Text = "Configuration";
			this.configuration.UseVisualStyleBackColor = true;
			//
			// menuStrip1
			//
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(800, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			//
			// toolStripMenuItem1
			//
			this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.options,
            this.testChangeService,
            this.toolStripMenuItem7});
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(46, 20);
			this.toolStripMenuItem1.Text = "Tools";
			//
			// testChangeService
			//
			this.testChangeService.Name = "testChangeService";
			this.testChangeService.Size = new System.Drawing.Size(180, 22);
			this.testChangeService.Text = "!!!!";
			this.testChangeService.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
			//
			// options
			//
			this.options.Name = "options";
			this.options.Size = new System.Drawing.Size(180, 22);
			this.options.Text = "Options";
			this.options.Click += new System.EventHandler(this.Options_Click);
			//
			// toolStripMenuItem7
			//
			this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripMenuItem9});
			this.toolStripMenuItem7.Name = "toolStripMenuItem7";
			this.toolStripMenuItem7.Size = new System.Drawing.Size(180, 22);
			this.toolStripMenuItem7.Text = "toolStripMenuItem7";
			//
			// toolStripMenuItem8
			//
			this.toolStripMenuItem8.Name = "toolStripMenuItem8";
			this.toolStripMenuItem8.Size = new System.Drawing.Size(180, 22);
			this.toolStripMenuItem8.Text = "toolStripMenuItem8";
			//
			// toolStripMenuItem9
			//
			this.toolStripMenuItem9.Name = "toolStripMenuItem9";
			this.toolStripMenuItem9.Size = new System.Drawing.Size(180, 22);
			this.toolStripMenuItem9.Text = "toolStripMenuItem9";
			//
			// toolStripMenuItem2
			//
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(125, 20);
			this.toolStripMenuItem2.Text = "toolStripMenuItem2";
			//
			// toolStripMenuItem3
			//
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(125, 20);
			this.toolStripMenuItem3.Text = "toolStripMenuItem3";
			//
			// toolStripMenuItem4
			//
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(125, 20);
			this.toolStripMenuItem4.Text = "toolStripMenuItem4";
			//
			// MainView
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
			this.Name = "MainView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SIM.UI";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox equipments;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage summary;
        private System.Windows.Forms.TabPage configuration;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem testChangeService;
        private System.Windows.Forms.ToolStripMenuItem options;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
    }
}

