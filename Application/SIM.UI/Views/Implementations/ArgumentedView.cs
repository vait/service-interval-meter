﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
	public abstract class ArgumentedView<TArgs> : BaseView where TArgs : class, INotifyPropertyChanged
	{
		public TArgs Args { get; set; }

		protected ArgumentedView(ApplicationContext context) : base(context)
		{
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			SetDataBindings();
		}

		protected abstract void SetDataBindings();
	}
}
