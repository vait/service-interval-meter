﻿using SIM.UI.Models;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIM.UI.Views.Implementations
{
	public partial class ServiceView : ArgumentedView<ServiceUiObject>, IServiceView
	{
		public ServiceView(ApplicationContext context) : base(context)
		{
			InitializeComponent();
		}

		public event Func<ServiceUiObject, Task> Save;
		public event Func<Task> Cancel;

		protected override void SetDataBindings()
		{
			dateService.DataBindings.Add(nameof(dateService.Value), Args, nameof(Args.LastServiceDate));
		}

		private void SaveOnClick(object sender, EventArgs e)
		{
			Close();
		}
	}
}
