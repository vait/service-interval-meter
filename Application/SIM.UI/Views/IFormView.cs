﻿using System.ComponentModel;

namespace SIM.UI.Views
{
	public interface IFormView : IView
	{
	}

	public interface IFormView<TArgs> : IFormView, IView<TArgs> where TArgs : INotifyPropertyChanged
	{
	}
}
