﻿namespace SIM.UI.Views
{
	public interface ILoaderView: IClosableView
	{
		void ShowInformation(string text);
	}
}
