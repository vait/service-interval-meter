﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SIM.UI.Views
{
	public interface IEditView<TArgs> : IFormView<TArgs> where TArgs : INotifyPropertyChanged
	{
		event Func<TArgs, Task> Save;
		event Func<Task> Cancel;
	}
}
