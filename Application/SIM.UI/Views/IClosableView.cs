﻿namespace SIM.UI.Views
{
	public interface IClosableView : IView
	{
		void Close();

		void SafeClose();
	}
}
