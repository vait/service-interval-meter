﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SIM.UI.Views
{
	public interface IView
	{
		void ShowView();

		event Action OnClose;

		event Func<Task> OnLoadedAsync;
	}

	public interface IView<TArgs> : IView where TArgs : INotifyPropertyChanged
	{
		TArgs Args { set; }
	}
}
