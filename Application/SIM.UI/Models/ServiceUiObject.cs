﻿using System;

namespace SIM.UI.Models
{
	/// <summary>
	/// Информация о сервисном обслуживани.
	/// </summary>
	public class ServiceUiObject : NotifyPropertyChangedObject
	{
		/// <summary>
		/// Вид спорта.
		/// </summary>
		private DateTime _lastServiceDate;

		/// <summary>
		/// Дата последнего технического обслужвания.
		/// </summary>
		public DateTime LastServiceDate
		{
			get =>
				_lastServiceDate;

			set
			{
				if (value != _lastServiceDate)
				{
					_lastServiceDate = value;
					NotifyPropertyChanged();
				}
			}
		}
	}
}
