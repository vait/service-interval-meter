﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SIM.UI.Models
{
	public abstract class NotifyPropertyChangedObject : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
