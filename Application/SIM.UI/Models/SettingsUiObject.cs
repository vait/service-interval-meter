﻿using System;

namespace SIM.UI.Models
{
	/// <summary>
	/// Настройки системы.
	/// </summary>
	public class SettingsUiObject : NotifyPropertyChangedObject
	{
		private string _login;
		private string _password;
		private string _stLogin;
		private string _stPassword;
		private string _equipments;
		private string _moves;
		private DateTimeOffset? _startDate;
		private DateTimeOffset? _lastDate;

		/// <summary>
		/// Имя пользователя на сайте Movescont.com
		/// </summary>
		public string Login
		{
			get => _login;

			set
			{
				if (value != _login)
				{
					_login = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Пароль на сайте Movescont.com
		/// </summary>
		public string Password
		{
			get => _password;

			set
			{
				if (value != _password)
				{
					_password = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Имя пользователя на сайте sport-tracker.com
		/// </summary>
		public string StLogin
		{
			get => _stLogin;

			set
			{
				if (value != _stLogin)
				{
					_stLogin = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Пароль на сайте sport-tracker.com
		/// </summary>
		public string StPassword
		{
			get => _stPassword;

			set
			{
				if (value != _stPassword)
				{
					_stPassword = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Путь к файлу с оборудованием.
		/// </summary>
		public string Equipments
		{
			get => _equipments;

			set
			{
				if (value != _equipments)
				{
					_equipments = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Путь к файлу с активностями.
		/// </summary>
		public string Moves
		{
			get => _moves;

			set
			{
				if (value != _moves)
				{
					_moves = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Дата начала приода анализа.
		/// </summary>
		public DateTimeOffset? StartDate
		{
			get => _startDate;

			set
			{
				if (value != _startDate)
				{
					_startDate = value;
					NotifyPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Дата окончания приода анализа.
		/// </summary>
		public DateTimeOffset? LastDate
		{
			get => _lastDate;

			set
			{
				if (value != _lastDate)
				{
					_lastDate = value;
					NotifyPropertyChanged();
				}
			}
		}
	}
}

