﻿using SIM.Core.Models;

namespace SIM.UI.Models
{
	public class Equipment : NotifyPropertyChangedObject
	{
		/// <summary>
		/// Вид спорта.
		/// </summary>
		private ActivityType _activityType;
		public ActivityType ActivityType
		{
			get =>
				_activityType;

			set
			{
				if (value != _activityType)
				{
					_activityType = value;
					NotifyPropertyChanged();
				}
			}
		}
	}
}
