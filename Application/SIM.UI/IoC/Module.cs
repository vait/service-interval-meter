﻿using Microsoft.Extensions.DependencyInjection;
using SIM.UI.Controllers;
using SIM.UI.Presenters;
using SIM.UI.Presenters.Implementations;
using SIM.UI.Views;
using SIM.UI.Views.Implementations;
using System.Windows.Forms;

namespace SIM.UI.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection WithUI(this IServiceCollection serviceCollection)
		{
			return
				serviceCollection
					.AddScoped<IMainView, MainView>()
					.AddScoped<ILoaderView, LoaderView>()
					.AddTransient<IServiceView, ServiceView>()
					.AddTransient<ISettingsView, SettingsView>()
					.AddScoped<IMainPresenter, MainPresenter>()
					.AddScoped<ILoaderPresenter, LoaderPresenter>()
					.AddScoped<IServicePresenter, ServicePresenter>()
					.AddScoped<ISettingsPresenter, SettingsPresenter>()
					.AddSingleton(new ApplicationContext())
					.AddSingleton<ApplicationController, ApplicationController>()
				;
		}
	}
}
