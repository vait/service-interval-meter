﻿namespace SIM.UI.Presenters
{
	public interface IPresenter
	{
		void Run();
	}

	public interface IPresenter<TArgs>
	{
		void Run(TArgs args);
	}
}
