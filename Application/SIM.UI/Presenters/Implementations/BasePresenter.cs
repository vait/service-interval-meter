﻿using SIM.UI.Controllers;
using SIM.UI.Views;

namespace SIM.UI.Presenters.Implementations
{
	public abstract class BasePresenter<TView> where TView: IView
	{
		private TView _view;
		protected abstract TView ViewGetter();
		protected readonly ApplicationController AppController;

		protected TView View
		{
			get
			{
				if (_view == null)
				{
					_view = ViewGetter();
					View.OnClose += Close;
				}

				return _view;
			}
		}

		protected BasePresenter(ApplicationController appController)
		{
			AppController = appController;
		}

		protected virtual void Close()
		{
			_view = default;
		}
	}
}
