﻿using System;
using System.Threading.Tasks;
using SIM.UI.Controllers;
using SIM.UI.Views;

namespace SIM.UI.Presenters.Implementations
{
	public class LoaderPresenter: SimplePreseneter<ILoaderView>, ILoaderPresenter
	{
		private Func<Action<string>, Task> _loader;
		private readonly ILoaderView _view;

		public LoaderPresenter(ILoaderView loaderView, ApplicationController controller): base(controller)
		{
			_view = loaderView;
			View.OnLoadedAsync += OnLoadedAsync;
		}

		private async Task OnLoadedAsync()
		{
			var loaderInternal = _loader;
			if (loaderInternal != null)
			{
				await loaderInternal(View.ShowInformation).ConfigureAwait(false);
			}

			View.SafeClose();
		}

		public void Run(Func<Action<string>, Task> loader)
		{
			_loader = loader;
			base.Run();
		}

		protected override ILoaderView ViewGetter()
		{
			return _view;
		}
	}
}
