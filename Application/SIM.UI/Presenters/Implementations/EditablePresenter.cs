﻿using SIM.UI.Controllers;
using SIM.UI.Views;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SIM.UI.Presenters.Implementations
{
	public abstract class EditablePresenter<TView, TArgs> : BasePresenter<TView>, IPresenter<TArgs> where TView : IEditView<TArgs> where TArgs : INotifyPropertyChanged
	{
		protected TArgs Args;
		protected EditablePresenter(ApplicationController appController) : base(appController)
		{
			View.Cancel += View_Cancel;
			View.Save += View_Save;
		}

		public void Run(TArgs args)
		{
			Args = args;
			View.Args = Args;
			View.ShowView();
		}

		protected abstract Task View_Save(TArgs args);

		protected abstract Task View_Cancel();

	}
}
