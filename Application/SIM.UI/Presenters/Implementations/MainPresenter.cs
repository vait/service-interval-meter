﻿using SIM.Core;
using SIM.UI.Controllers;
using SIM.UI.Models;
using SIM.UI.Views;
using SIM.Utils;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SIM.UI.Presenters.Implementations
{
	public class MainPresenter : SimplePreseneter<IMainView>, IMainPresenter
	{
		private readonly IMainView _view;
		private readonly IMovesParser _movesParser;
		private readonly ILoaderData _loader;

		public MainPresenter(IMainView mainView, ApplicationController appController, ILoaderData loader, IMovesParser movesParser) :
			base(appController)
		{
			_loader = loader;
			_movesParser = movesParser;
			_view = mainView;
			View.OnClose += Close;
			View.OnLoadedAsync += LoadAsync;
			View.ChangeService += ChangeService;
			View.ChangeSettings += ChangeSettings;
			View.Test += ViewOnTest;


			//_mainView.

		}

		private void ViewOnTest(Func<Action<string>, Task> obj)
		{
			AppController.RunLoader(obj);
		}


		private void ChangeService()
		{
			var obj = new ServiceUiObject
			{
				LastServiceDate = DateTime.Now
			};
			AppController.Run<IServicePresenter, ServiceUiObject>(obj);
		}

		private void ChangeSettings()
		{

			AppController.Run<ISettingsPresenter>();
		}

		private Task LoadAsync()
		{
			AppController.RunLoader(async (show) =>
			{
				show("Loading equipments...");
				await LoadEquipmentsAsync().ConfigureAwait(false);
				show("Loading data...");
				await LoadDataAsync(show).ConfigureAwait(false);
			});

			return Task.CompletedTask;
		}

		private async Task LoadDataAsync(Action<string> showInfo)
		{
			//await Task.Delay(5000).ConfigureAwait(false);
			showInfo(AppController.Config.MovesPath);
			//await Task.Delay(5000).ConfigureAwait(false);
			//showInfo("456");
			await Task.Delay(1000).ConfigureAwait(false);
		}

		private async Task LoadEquipmentsAsync()
		{
			var path = AppController.Config.EquipmentPath;
			if (!File.Exists(path))
			{
				throw new FileNotFoundException("Can't load equipments", path);
			}

			var equipmentsInternal = await _loader.LoadAsync<Equipment[]>(path).ConfigureAwait(false);


		}

		protected override IMainView ViewGetter()
		{
			return _view;
		}
	}
}
