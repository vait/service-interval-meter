﻿using SIM.UI.Controllers;
using SIM.UI.Models;
using SIM.UI.Views;
using System.Threading.Tasks;

namespace SIM.UI.Presenters.Implementations
{
	public class ServicePresenter : EditablePresenter<IServiceView, ServiceUiObject>, IServicePresenter
	{
		public ServicePresenter(ApplicationController appController) :
			base(appController)
		{
		}

		protected override IServiceView ViewGetter()
		{
			return AppController.ResolveView<IServiceView>();
		}

		protected override Task View_Cancel()
		{
			return Task.CompletedTask;
		}

		protected override Task View_Save(ServiceUiObject newArgs)
		{
			return Task.CompletedTask;
		}
	}
}
