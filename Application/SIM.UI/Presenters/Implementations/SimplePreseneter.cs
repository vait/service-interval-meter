﻿using SIM.UI.Controllers;
using SIM.UI.Views;

namespace SIM.UI.Presenters.Implementations
{
	public abstract class SimplePreseneter<TView>: BasePresenter<TView> where TView: IView
	{
		protected SimplePreseneter(ApplicationController appController) : base(appController)
		{
		}

		public void Run()
		{
			View.ShowView();
		}
	}
}
