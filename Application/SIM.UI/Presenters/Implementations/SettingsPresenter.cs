﻿using SIM.Application.Core.Dto;
using SIM.UI.Controllers;
using SIM.UI.Models;
using SIM.UI.Views;
using SIM.Utils;
using System;
using System.Threading.Tasks;

namespace SIM.UI.Presenters.Implementations
{
	public class SettingsPresenter : EditablePresenter<ISettingsView, SettingsUiObject>, ISettingsPresenter
	{
		private readonly ISaverData _saverData;

		public SettingsPresenter(
			ApplicationController appController,
			ISaverData saverData
			) :
			base(appController)
		{
			_saverData = saverData;
		}

		protected override async Task View_Save(SettingsUiObject newArgs)
		{
			var privateConfig = new Configuration
			{
				Login = newArgs.Login,
				Password = newArgs.Password,
				SportTrackerLogin = newArgs.StLogin,
				SportTrackerPassword = newArgs.StPassword
			};

			var publicConfig = new Configuration
			{
				EquipmentPath = newArgs.Equipments,
				MovesPath = newArgs.Moves,
				LastDate = newArgs.LastDate,
				StartDate = newArgs.StartDate
			};

			await _saverData.SaveAsync(privateConfig, AppController.PrivateConfigFile).ConfigureAwait(false);
			await _saverData.SaveAsync(publicConfig, AppController.PublicConfigFile).ConfigureAwait(false);
		}

		protected override Task View_Cancel()
		{
			return Task.CompletedTask;
		}

		protected override ISettingsView ViewGetter()
		{
			return AppController.ResolveView<ISettingsView>();
		}

		public void Run()
		{
			var config = AppController.Config;
			Args = new SettingsUiObject
			{
				Login = config.Login,
				Moves = config.MovesPath,
				LastDate = config.LastDate?.Date ?? null,
				Equipments = config.EquipmentPath,
				Password = config.Password,
				StLogin = config.SportTrackerLogin,
				StPassword = config.SportTrackerPassword,
				StartDate = config.StartDate?.Date ?? DateTime.MinValue
			};

			View.Args = Args;
			View.ShowView();
		}
	}
}
