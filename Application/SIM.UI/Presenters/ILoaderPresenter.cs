﻿using System;
using System.Threading.Tasks;

namespace SIM.UI.Presenters
{
	public interface ILoaderPresenter: IPresenter
	{
		void Run(Func<Action<string>, Task> loader);
	}
}
