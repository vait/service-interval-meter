﻿using SIM.Application.Core.Configuration;
using SIM.Application.Core.Dto;
using SIM.Core;
using SIM.Core.Models;
using SIM.Network;
using SIM.Statistic;
using SIM.Statistic.Models;
using SIM.Utils;
using SIM.Utils.Loaders;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SIM
{
	internal class Program
	{
		private static readonly ILoaderData _loader = new SimpleJsonLoaderData();
		private static readonly IMovesParser _movesParser = new MovescountParser();

		private static async Task Main(string[] args)
		{
			var configuration = args.GetConfiguration();
			var (moves, equipments) = await LoadDataAsync(configuration, CancellationToken.None).ConfigureAwait(false);
			var statistics = GetStatistic(moves, equipments);

			PrintStatistic(statistics);
		}

		private static ValueTask<Equipment[]> LoadEquipmentAsync(string path)
		{
			if (!File.Exists(path))
			{
				throw new FileNotFoundException("Can't load equipments", path);
			}

			return _loader.LoadAsync<Equipment[]>(path);
		}

		private static async ValueTask<(Move[] moves, Equipment[] equipments)> LoadDataAsync(Configuration configuration, CancellationToken ct)
		{
			var equipments = await LoadEquipmentAsync(configuration.EquipmentPath).ConfigureAwait(false);
			var minServiceDate = equipments.Min(x => x.LastServiceDate);
			var startDate = configuration.StartDate < minServiceDate ? minServiceDate : configuration.StartDate;
			Move[] moves;

			if (!File.Exists(configuration.MovesPath))
			{
				using (var client = new MovescountLoader())
				{
					moves = await client.LoadAsync(configuration.Login, configuration.Password, startDate, configuration.LastDate, ct).ConfigureAwait(false);
				}

				if (!string.IsNullOrWhiteSpace(configuration.SportTrackerLogin) && !string.IsNullOrWhiteSpace(configuration.SportTrackerPassword))
				{
					Move[] stMoves;
					var lastDate = moves.Max(x => x.StartDate).AddDays(1); //Начинаем со следующего дня после закрытия MovesCount
					using (var client = new SportTrackerLoader())
					{
						stMoves = await client.LoadAsync(configuration.SportTrackerLogin, configuration.SportTrackerPassword, lastDate, configuration.LastDate, ct).ConfigureAwait(false);
					}

					moves = moves.Concat(stMoves).ToArray();
				}
			}
			else
			{
				using (var stream = new StreamReader(configuration.MovesPath))
				{
					moves = await _movesParser.GetMoviesAsync(stream, startDate, configuration.LastDate, ct).ConfigureAwait(false);
				}
			}

			return (moves, equipments);
		}

		private static ServiceIntervalStatistic[] GetStatistic(Move[] moves, Equipment[] equipments)
		{
			var statistic = new StatisticProcessor();
			return statistic.GetStatistic(moves, equipments);
		}

		private static void PrintStatistic(ServiceIntervalStatistic[] statistics)
		{
			foreach (var st in statistics.OrderBy(x => x.Equipment.Name).ThenBy(x => x.Duration).ThenBy(x => x.Distance).ThenBy(x => x.Equipment.Name))
			{
				Console.WriteLine($"[{st.Equipment.Name}] Distance: {st.Distance}; Duration: {(long)st.Duration.TotalHours}:{st.Duration.Minutes}:{st.Duration.Seconds}; MovesCount: {st.MovesCount}");
			}
		}
	}
}
