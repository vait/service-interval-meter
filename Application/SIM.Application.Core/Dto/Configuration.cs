using System;

namespace SIM.Application.Core.Dto
{
	/// <summary>
	/// Конфигурация приложения.
	/// </summary>
	public class Configuration
	{
		/// <summary>
		/// Путь к файлу с перемещениями.
		/// </summary>
		public string MovesPath { get; set; }

		/// <summary>
		/// Путь к файлу с оборудованием.
		/// </summary>
		public string EquipmentPath { get; set; }

		/// <summary>
		/// Дата начала обработки перемещений.
		/// </summary>
		public DateTimeOffset? StartDate { get; set; }

		/// <summary>
		/// Дата окончания обработки перемещений.
		/// </summary>
		public DateTimeOffset? LastDate { get; set; }

		/// <summary>
		/// Имя пользователя.
		/// </summary>
		public string Login { get; set; }

		/// <summary>
		/// Пароль.
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// Имя пользователя https://www.sports-tracker.com/.
		/// </summary>
		public string SportTrackerLogin { get; set; }

		/// <summary>
		/// Пароль https://www.sports-tracker.com/.
		/// </summary>
		public string SportTrackerPassword { get; set; }
	}
}
