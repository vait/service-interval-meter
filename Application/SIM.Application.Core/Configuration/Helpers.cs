﻿using Microsoft.Extensions.Configuration;
using SIM.Application.Core.Configuration.CommandLineUtils;

namespace SIM.Application.Core.Configuration
{
	public static class Helpers
	{
		public static IConfigurationBuilder AddCustomCommandLine(
			this IConfigurationBuilder configurationBuilder,
			string[] args)
		{
			configurationBuilder.Add(new CommandLineConfigurationSource { Args = args });
			return configurationBuilder;
		}

		public static Dto.Configuration GetConfiguration(this string[] args)
		{
			var config = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json", true, false)
				.AddCustomCommandLine(args)
				.Build();

			return config.Get<Dto.Configuration>();
		}
	}
}
