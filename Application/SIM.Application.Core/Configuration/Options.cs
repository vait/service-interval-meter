﻿using CommandLine;
using System;

namespace SIM.Application.Core.Configuration
{
	internal class Options
	{
		[Option('m', "moves", Required = true, HelpText = "Path to a file with moves downloaded from http://www.movescount.com/Move/MoveList.")]
		public string MovesPath { get; set; }

		[Option('e', "equipment", Required = true, HelpText = "Path to a JSON file with equipments.")]
		public string EquipmentPath { get; set; }

		[Option("start-date", Required = false, HelpText = "The start date of the parsing.")]
		public DateTimeOffset? StartDate { get; set; }

		[Option("last-date", Required = false, HelpText = "The ending date of the parsing.")]
		public DateTimeOffset? LastDate { get; set; }

		[Option('l', "login", Required = false, HelpText = "Login from the www.movescount.com")]
		public string Login { get; set; }

		[Option('p', "password", Required = false, HelpText = "Password from the www.movescount.com")]
		public string Password { get; set; }

		[Option("stlogin", Required = false, HelpText = "Login from the https://www.sports-tracker.com/")]
		public string SportTrackerLogin { get; set; }

		[Option("stpassword", Required = false, HelpText = "Password from the https://www.sports-tracker.com/")]
		public string SportTrackerPassword { get; set; }
	}
}
