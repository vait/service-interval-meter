using CommandLine;
using CommandLine.Text;
using SIM.Utils.Models;
using System;

namespace SIM.Application.Core.Configuration.CommandLineUtils
{
	internal class Parser
	{
		public ActionResult<Dto.Configuration> Parse(string[] args)
		{
			var parser = new CommandLine.Parser(with => with.HelpWriter = null);

			var parserResult = parser.ParseArguments<Options>(args);

			return parserResult
				.MapResult(options =>
					{
						var cfg = new Dto.Configuration
						{
							EquipmentPath = options.EquipmentPath,
							LastDate = options.LastDate,
							MovesPath = options.MovesPath,
							StartDate = options.StartDate,
							Login = options.Login,
							Password = options.Password,
							SportTrackerLogin = options.SportTrackerLogin,
							SportTrackerPassword = options.SportTrackerPassword
						};
						return new ActionResult<Dto.Configuration>(cfg);
					},
					errors =>
					{
						var helpText = HelpText.AutoBuild(parserResult,
							h =>
							{
								h.AutoHelp = false;
								h.AutoVersion = false; //hide --version
								return HelpText.DefaultParsingErrorsHandler(parserResult, h);
							},
							e => e);
						Console.WriteLine(helpText);
						return ActionResult<Dto.Configuration>.UnSuccess;
					});
		}
	}
}
