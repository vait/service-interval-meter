﻿using Microsoft.Extensions.Configuration;

namespace SIM.Application.Core.Configuration.CommandLineUtils
{
	public class CommandLineConfigurationSource : IConfigurationSource
	{
		public string[] Args { get; set; }

		public IConfigurationProvider Build(IConfigurationBuilder builder)
		{
			return new CommandLineConfigurationProvider(Args);
		}
	}
}
