﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using SIM.Utils.Helpers;

namespace SIM.Application.Core.Configuration.CommandLineUtils
{
	public class CommandLineConfigurationProvider: ConfigurationProvider
	{
		protected string[] Args { get; private set; }

		public CommandLineConfigurationProvider(string[] args)
		{
			Args = args ?? throw new ArgumentNullException(nameof(args));
		}

		public override void Load()
		{
			var data = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			var parser = new Parser();
			var configuration = parser.Parse(Args);

			if (configuration == null || !configuration.IsSuccess)
			{
				Data = data;
				return;
			}

			Data = configuration.Data.ToDictionary();
		}
	}
}
