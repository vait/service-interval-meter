﻿using Microsoft.Extensions.Configuration;

namespace SIM.Infrastructure.Implementations
{
	internal class SIMConfigurationService : ISIMConfigurationService
	{
		private readonly IConfiguration _configuration;

		public SIMConfigurationService(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public T Get<T>(string key)
		{
			return _configuration.GetSection(key).Get<T>();
		}

		public T Get<T>()
		{
			var a = _configuration.Get<T>();


			return a;
		}
	}
}
