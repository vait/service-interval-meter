﻿using Microsoft.Extensions.DependencyInjection;
using SIM.Infrastructure.Implementations;

namespace SIM.Infrastructure.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection UseSIMInfrastructure(this IServiceCollection serviceCollection)
		{
			return
				serviceCollection
					.AddSingleton<ISIMConfigurationService, SIMConfigurationService>()
				;
		}
	}
}
