﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace SIM.Infrastructure
{
	public static class ConfigurationUtils
	{
		public static IConfigurationBuilder AddSharedJsonFile(this IConfigurationBuilder config, HostBuilderContext hostingContext, int level)
		{
			var env = hostingContext.HostingEnvironment;
			var sharedFolder = env.ContentRootPath;

			int i = 0;
			while (i < level)
			{
				sharedFolder = Path.Combine(sharedFolder, "..");
				++i;
			}

			var sharedAppsettings = Path.Combine(sharedFolder, "SolutionItems", $"appsettings.{env.EnvironmentName}.json");

			return config.AddJsonFile(sharedAppsettings, false, false);
		}
	}
}
