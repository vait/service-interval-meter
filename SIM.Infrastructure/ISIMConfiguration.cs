﻿namespace SIM.Infrastructure
{
	public interface ISIMConfigurationService
	{
		T Get<T>();

		T Get<T>(string key);
	}
}
