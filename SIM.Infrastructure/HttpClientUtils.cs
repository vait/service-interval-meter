﻿using Microsoft.Extensions.DependencyInjection;
using System.Net;

namespace SIM.Infrastructure
{
	public static class HttpClientUtils
	{
		public static string Name = "base";

		public static IServiceCollection AddDefaultHttpClient(this IServiceCollection serviceCollection)
		{
			serviceCollection
			   .AddHttpClient(Name)
			   .ConfigurePrimaryHttpMessageHandler(() =>
			   {
				   var handler = new HttpClientHandler
				   {
					   UseCookies = true,
					   AllowAutoRedirect = true,
					   CookieContainer = new CookieContainer(),
					   AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate | DecompressionMethods.Brotli,
					   ServerCertificateCustomValidationCallback = (_, _, _, _) => true,
				   };

				   return handler;
			   });

			return serviceCollection;
		}
	}
}
