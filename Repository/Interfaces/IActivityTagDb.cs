﻿using Repository.Models;

namespace Repository.Interfaces
{
	public interface IActivityTagDb : IKeyLessRepository<ActivityTagDao>
	{
	}
}
