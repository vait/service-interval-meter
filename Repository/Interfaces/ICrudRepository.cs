﻿using Repository.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
	public interface ICrudRepository<TEntity, TKey> : IReadOnlyRepository<TEntity, TKey> where TEntity : EntityBase<TKey>
	{
		Task<TEntity> CreateAsync(TEntity entity, CancellationToken ct);

		Task<TEntity> UpdateAsync(TEntity entity, CancellationToken ct);
	}
}
