﻿using Repository.Models;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
	public interface IReadOnlyRepository<TEntity, in TKey> where TEntity : EntityBase<TKey>
	{
		Task<TEntity> FindAsync(TKey id, CancellationToken ct);

		Task<TEntity[]> FindAllAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken ct);

		Task<TEntity[]> FindAllAsync<TProperty>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TProperty>> includes, CancellationToken ct);
	}
}
