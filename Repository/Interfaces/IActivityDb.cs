﻿using Repository.Models;

namespace Repository.Interfaces
{
	public interface IActivityDb : ICrudRepository<ActivityDao, long>
	{
	}
}
