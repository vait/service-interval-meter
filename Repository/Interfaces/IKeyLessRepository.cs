﻿using Repository.Models;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
	public interface IKeyLessRepository<TEntity> where TEntity : KeyLessEntityBase
	{
		Task<TEntity> CreateAsync(TEntity entity);

		Task DeleteAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken ct);
	}
}
