﻿using Repository.Models;

namespace Repository.Interfaces
{
	public interface ITagDb : ICrudRepository<TagDao, long>
	{
	}
}
