﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
	[Table("equipment_tags")]
	public class EquipmentTagDao
	{
		[Column("tag_id")]
		public long TagId { get; set; }

		[Column("equipment_id")]
		public long EquipmentId { get; set; }

		[ForeignKey(nameof(TagId))]
		public TagDao Tag { get; set; }

		[ForeignKey(nameof(EquipmentId))]
		public EquipmentDao Equipment { get; set; }
	}
}
