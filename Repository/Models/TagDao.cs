﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
	[Table("tag")]
	public class TagDao : EntityBase<long>
	{
		[Column("tag_id")]
		override public long Id { get; set; }

		[Column("tag")]
		public string Name { get; set; }
	}
}
