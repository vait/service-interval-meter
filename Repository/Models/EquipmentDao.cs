﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
	[Table("equipment")]
	public class EquipmentDao
	{
		[Column("equipment_id")]
		public long Id { get; set; }

		[Column("name")]
		public string Name { get; set; }


	}
}
