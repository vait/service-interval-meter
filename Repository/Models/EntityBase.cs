﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
	public abstract class EntityBase<T>
	{
		[Key]
		public virtual T Id { get; set; }
	}
}
