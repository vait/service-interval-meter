﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
	[Table("activity")]
	public class ActivityDao : EntityBase<long>
	{
		[Column("activity_id")]
		public override long Id { get; set; }

		[Column("activity_type_id")]
		public ActivityTypeDao Type { get; set; }

		[Column("start_date")]
		public DateTime Start { get; set; }

		[Column("duration")]
		public double Duration { get; set; }

		[Column("distance")]
		public double Distance { get; set; }

		[Column("workout_key")]
		public string WorkoutKey { get; set; }

		public ICollection<ActivityTagDao> Tags { get; set; }
	}
}
