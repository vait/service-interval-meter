﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
	[Table("activity_tags")]
	public class ActivityTagDao : KeyLessEntityBase
	{
		[Column("tag_id")]
		public long TagId { get; set; }

		[Column("activity_id")]
		public long ActivityId { get; set; }

		[ForeignKey(nameof(TagId))]
		public TagDao Tag { get; set; }

		[ForeignKey(nameof(ActivityId))]
		public ActivityDao Activity { get; set; }
	}
}
