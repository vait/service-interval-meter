﻿using Repository.Context;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
	internal class ActivityDb : CrudRepository<ActivityDao, long>, IActivityDb
	{
		public ActivityDb(IContextFactory contextFactory) : base(contextFactory)
		{
		}
	}
}
