﻿using Repository.Context;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
	internal class ActivityTagDb : KeyLessRepository<ActivityTagDao>, IActivityTagDb
	{
		public ActivityTagDb(IContextFactory contextFactory) : base(contextFactory)
		{
		}
	}
}
