﻿using Repository.Context;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Repository.Implementations
{
	internal class KeyLessRepository<TEntity> : BaseRepository, IKeyLessRepository<TEntity> where TEntity : KeyLessEntityBase
	{
		public KeyLessRepository(IContextFactory contextFactory) : base(contextFactory)
		{
		}

		public async Task<TEntity> CreateAsync(TEntity entity)
		{
			using (var context = CreateContext())
			{
				await context.AddAsync(entity);
				await context.SaveChangesAsync();
				return entity;
			}
		}

		public async Task DeleteAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				await context
					.GetQuery<TEntity>()
					.Where(predicate)
					.DeleteAsync(ct);
			}
		}
	}
}
