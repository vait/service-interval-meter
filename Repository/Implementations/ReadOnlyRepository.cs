﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.Implementations
{
	internal class ReadOnlyRepository<TEntity, TKey> : BaseRepository, IReadOnlyRepository<TEntity, TKey> where TEntity : EntityBase<TKey>
	{
		public ReadOnlyRepository(IContextFactory contextFactory) : base(contextFactory)
		{
		}

		public async Task<TEntity[]> FindAllAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				var query = context
					.GetQuery<TEntity>()
					.ApplyFilter(predicate);

				var result = await query.ToArrayAsync(ct);
				return result;
			}
		}

		public async Task<TEntity[]> FindAllAsync<TProperty>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TProperty>> includes, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				var query = context
					.GetQuery<TEntity>()
					.ApplyFilter(predicate);

				if (includes != null)
				{
					query = query.Include(includes);
				}

				var result = await query.ToArrayAsync(ct);
				return result;
			}
		}

		public async Task<TEntity> FindAsync(TKey id, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				var query = context
					.GetQuery<TEntity>()
					.Where(x => new[] { id }.Contains(x.Id));

				return await query.SingleOrDefaultAsync(ct);
			}
		}

		/*		private IQueryable<TEntity> CreateFindAllQuery<TSort>(
				   DbContext context,
				   Expression<Func<TEntity, bool>> predicate)
				{

					var query = context
					   .GetQuery<TEntity>()
					   .ApplyFilter(predicate);

					if (position.HasValue || count.HasValue)
					{
						query = sortExpression == null
						   ? query.Paginate(position, count, DefaultSortExpression, sortType)
						   : query.Paginate(position, count, sortExpression, sortType);
					}

					return query
					   .FixIntCast()
					   .IncludeMultiple(includes);
				}*/
	}
}
