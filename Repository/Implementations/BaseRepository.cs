﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;

namespace Repository.Implementations
{
	// <summary>
	/// Represents base class for repository.
	/// </summary>
	internal abstract class BaseRepository
	{
		/// <summary>
		/// Gets parameters of connection to database.
		/// </summary>
		private readonly IContextFactory _contextFactory;

		/// <summary>
		/// .ctor
		/// </summary>
		protected BaseRepository(IContextFactory contextFactory)
		{
			_contextFactory = contextFactory;
		}

		/// <summary>
		/// Returns db context.
		/// </summary>
		protected virtual DbContext CreateContext()
		{
			return _contextFactory.CreateContext();
		}
	}
}
