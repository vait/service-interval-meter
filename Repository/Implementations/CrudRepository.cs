﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Repository.Context;
using Repository.Interfaces;
using Repository.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.Implementations
{
	internal class CrudRepository<TEntity, TKey> : ReadOnlyRepository<TEntity, TKey>, ICrudRepository<TEntity, TKey> where TEntity : EntityBase<TKey>
	{
		public CrudRepository(IContextFactory contextFactory) : base(contextFactory)
		{
		}

		public async Task<TEntity> CreateAsync(TEntity entity, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				await context.AddAsync(entity, cancellationToken: ct);
				await context.SaveChangesAsync(ct);
				return entity;
			}
		}

		public async Task<TEntity> UpdateAsync(TEntity entity, CancellationToken ct)
		{
			using (var context = CreateContext())
			{
				entity = SetNullProps(entity, context.Model);

				context.Entry(entity).State = EntityState.Modified;
				await context.SaveChangesAsync(ct);
				return entity;
			}
		}

		private TEntity SetNullProps(TEntity entity, Microsoft.EntityFrameworkCore.Metadata.IModel model)
		{
			foreach (var p in GetNavigationProperties(model))
			{
				p.SetValue(entity, null);
			}

			return entity;
		}

		private IEnumerable<PropertyInfo> GetNavigationProperties(IModel model)
		{
			return model.FindEntityType(typeof(TEntity)).GetNavigations().Select(p => p.PropertyInfo);
		}
	}
}
