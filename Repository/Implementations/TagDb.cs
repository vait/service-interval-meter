﻿using Repository.Context;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
	internal class TagDb : CrudRepository<TagDao, long>, ITagDb
	{
		public TagDb(IContextFactory contextFactory) : base(contextFactory)
		{
		}
	}
}
