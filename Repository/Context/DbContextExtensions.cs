﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Context
{
	internal static class DbContextExtensions
	{
		public static IQueryable<T> GetQuery<T>(this DbContext context) where T : class
		{
			return context.Set<T>().AsNoTracking();
		}

		public static IQueryable<T> ApplyFilter<T>(this IQueryable<T> query, Expression<Func<T, bool>> predicate)
		{
			return predicate == null ? query : query.Where(predicate);
		}
	}
}
