﻿using Microsoft.EntityFrameworkCore;
using Repository.Models;
using System;

namespace Repository.Context
{
	internal class MySqlContext : DbContext
	{
		public DbSet<TagDao> Tags { get; set; }
		public DbSet<EquipmentDao> Equipments { get; set; }
		public DbSet<EquipmentTagDao> EquipmentTags { get; set; }
		public DbSet<ActivityDao> Activities { get; set; }
		public DbSet<ActivityTagDao> ActivityTags { get; set; }

		public MySqlContext(DbContextOptions options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<EquipmentTagDao>()
			.HasKey(nameof(EquipmentTagDao.TagId), nameof(EquipmentTagDao.EquipmentId));

			modelBuilder.Entity<ActivityTagDao>()
			.HasKey(nameof(ActivityTagDao.ActivityId), nameof(ActivityTagDao.TagId));
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
			=> optionsBuilder.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Error);
	}
}
