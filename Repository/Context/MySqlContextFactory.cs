﻿using Microsoft.EntityFrameworkCore;
using SIM.Infrastructure;

namespace Repository.Context
{
	internal class MySqlContextFactory : IContextFactory
	{
		private readonly ISIMConfigurationService _config;

		public MySqlContextFactory(ISIMConfigurationService config)
		{
			_config = config;
		}

		public DbContext CreateContext()
		{
			var dbConfig = _config.Get<DataBaseConnectionConfig>("DataBase");

			var builder = new DbContextOptionsBuilder<MySqlContext>();
			builder.
				UseMySQL($"server={dbConfig.Address};database={dbConfig.Name};user={dbConfig.Login};password={dbConfig.Password}");

			return new MySqlContext(builder.Options);
		}
	}
}
