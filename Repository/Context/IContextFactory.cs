﻿using Microsoft.EntityFrameworkCore;

namespace Repository.Context
{
	public interface IContextFactory
	{
		DbContext CreateContext();
	}
}
