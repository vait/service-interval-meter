﻿using Microsoft.Extensions.DependencyInjection;
using Repository.Context;
using Repository.Implementations;
using Repository.Interfaces;

namespace Repository.IoC
{
	/// <summary>
	/// DI инициализация
	/// </summary>
	public static class Module
	{
		public static IServiceCollection UseDb(this IServiceCollection serviceCollection)
		{
			return
				serviceCollection
					.AddSingleton<IContextFactory, MySqlContextFactory>()
					.AddSingleton<ITagDb, TagDb>()
					.AddSingleton<IActivityDb, ActivityDb>()
					.AddSingleton<IActivityTagDb, ActivityTagDb>()
				;
		}
	}
}
